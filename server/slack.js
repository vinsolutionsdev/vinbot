var https = require('https');
var comms = require('./comms.js');
var AppSettings = require('./settings.js').init();



module.exports.CheckSecure = function (messageData) {
	var retval = {};
	retval.validToken = AppSettings.Slack.Tokens.indexOf(messageData.token) >= 0
	retval.adminUser = AppSettings.Slack.AdminUserIDs.indexOf(messageData.userid) >= 0
	return retval;
}
	
module.exports.SendAlert = function (message, desitination, imageurl) {

    if (AppSettings.Slack.Enable == false) {
        comms.LogToConsole('Not sending message to slack, slack is disabled');
        comms.LogToConsole('I would have sent "' + message + '" to ' + desitination);
        return;
    }
    
    var payload = {
        text: message
    };
    if (desitination == 'vaw') {
        payload.channel = AppSettings.Slack.vawRoom;
        payload.username = "VIN-VAW-ServerMonitor";
    }
    else if (desitination == 'cms') {
        payload.channel = AppSettings.Slack.cmsRoom;
        payload.username = "VIN-CMS-ServerMonitor";
    }
    else {
        payload.channel =  desitination;
        payload.username = "VinBot";
        payload.icon_url = "https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2015-06-11/6277355030_df865b20bd592337fcfd_48.jpg";
    }
    if (imageurl != undefined && imageurl.length > 0) {
        payload.attachments = [
            {
                "image_url": imageurl
            }
        ];
    }

	var payloadString = JSON.stringify(payload);

    //comms.LogToConsole('length: ' + Buffer.byteLength(payloadString));
    
    var headers = {
        'Content-Type': 'application/json',
        'Content-Length': Buffer.byteLength(payloadString)
    };
    var options = {
        host: 'hooks.slack.com',
        port: 443,
        method: 'POST',
        path: AppSettings.Slack.ApiPath,
        headers: headers
    };

	// Setup the request.  The options parameter is
	// the object we defined above.
	var req = https.request(options, function(res) {
		res.setEncoding('utf-8');
		var responseString = '';
		res.on('data', function(data) {
			responseString += data;
		});
		res.on('end', function() {
			comms.LogToConsole(responseString);
		});
	});

	req.on('error', function(e) {
		comms.LogToConsole(e);
		// TODO: handle error.
	});

	req.write(payloadString);
	req.end();
}