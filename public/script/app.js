/* global angular */
(function(){
  var app = angular.module('VINServerMonitor', [
  'ngRoute',
  'VINServerMonitor.detail',
  'VINServerMonitor.simple',
  'ngSanitize'
]).config(['$routeProvider', 
  function($routeProvider) {
    $routeProvider.when('/Home', {
      templateUrl: '/pages/home.html'
    }).when('/Detail', {
      templateUrl: '/pages/detail.html'
    }).when('/Simple', {
      templateUrl: '/pages/simple.html'
    }).otherwise({
      redirectTo: '/Home'
    });
  }]);
  app.run(function ($rootScope, $location, $anchorScroll) {
      //when the route is changed scroll to the proper element.
      $rootScope.$on('$routeChangeSuccess', function (newRoute, oldRoute) {
          if ($location.hash()) $anchorScroll();
      });
  });
})();