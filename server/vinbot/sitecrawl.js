﻿
var http = require('http');
var _ = require('underscore');
var cheerio = require('cheerio');

var currentSitesRequested = 0;
var currentPagesRequested = 0;

module.exports = function crawlSite(url, callback){
    if (currentSitesRequested >= 1) {
        callback('Already crawling ' + currentSitesRequested + ' site(s)');
        return;
    }
    currentRequests++;
    makeRequest(url, callback);
}

function makeRequest(url, callback) {
    if (currentPagesRequested >= 20) {
        setTimeout(function () {
            makeRequest(option, url);
        }, 2000);
        return;
    }
    var request = http.request({ host: url, path: '/' }, function (res) {
        // var start = new Date().getTime();
        if (res.statusCode != 200) {
            console.log();
            //console.log(res);
            if (res.statusCode == 404) {
                if (res.headers.location) {//301
                    console.log('301 redirecting ' + res.req.path + ' to ' + res.headers.location);
                    addURLtoArray(res.headers.location, url.depth, res.req.path);
                    return false;
                }
                else {
                    console.warn(colors.red.underline("NOT FOUND"));
                }
            }
            else if (res.statusCode = 500) console.error(colors.red.underline("ERROR"));
            else console.info(res.statusCode + "RESPONSE");
            var path = 'http://' + option.host + res.req.path;
            console.log(colors.red('Path: ' + path));
            console.log(colors.red('Source: ' + url.source));
            console.log(colors.red('Found ' + url.depth + ' links deep from ' + options.path));
            console.log();
        }
        var data = '';
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            pendingRequests--;
            //console.log(data);
            count = count + 1;
            if ((count / alertEvery) % 1 === 0 || count == urls.length) {
                // console.log('#' + count + ' Depth: '+ url.depth + ' Url: ' + url.url + ' Source: ' + url.source);
                console.log('Processed: ' + count + '/' + urls.length);
            }
            var depth = url.depth;
            if (depth + 1 >= maxdepth) return;
            // var end = new Date().getTime();
            // var time = end - start;
            // if (time > alertAfter){
            // 	console.log(option.path +' Execution time: ' + time);
            // }
            var $ = cheerio.load(data);
            if ($('form').first().attr('action') == '500.aspx?error=true') {
                console.error(colors.red.underline("ERROR"));
                var path = 'http://' + option.host + res.req.path;
                console.log(colors.red('Path:' + path));
                console.log(colors.red('Source:' + url.source));
                console.log(colors.red('Found ' + url.depth + ' links deep from ' + options.path));
                console.log();
            }
            $('a').map(function (i, link) {
                var href = $(link).attr('href');
                addURLtoArray(href, depth, option.path);
            });
        });
    });
    request.on('error', function (e) {
        console.log(colors.red.underline('ERROR! ' + e.message));
    });
    request.end();
}

/*

var http = require('http');
var _ = require('underscore');
var cheerio = require('cheerio');
var websiteid = process.argv[2] || 3050;
var options = {
    host: process.argv[4] || 'ds.qa.vinsolutions.com',
    path: process.argv[3] || '/'
}
var urls = [];
var url = {
    hit: false,
    url: '',
    depth: 0,
    source: ''
}
var colors = require('colors/safe');
var alertEvery = 50;
// var alertAfter = 10000;
var maxdepth = 3;
var count = 0;
var cookie = 'websiteid=' + websiteid;
var headers = {
    'Cookie': cookie
};
var pendingRequests = 0;
var allowedPendingRequests = 100;

console.log(colors.green('Going to crawl websiteid ' + websiteid + ' on ' + options.host + options.path));
makeRequest(options, url);

function makeRequest(option, url) {
    if (pendingRequests >= allowedPendingRequests) {
        setTimeout(function () {
            makeRequest(option, url);
        }, 2000);
        return;
    }
    var match = _.find(urls, function (item) { return item.url === option.path })
    if (match) {
        match.hit = true;
    }
    pendingRequests++;
    var request = http.request({ headers: headers, host: option.host, path: encodeURI(decodeURI(option.path)) }, function (res) {
        // var start = new Date().getTime();
        if (res.statusCode != 200) {
            console.log();
            //console.log(res);
            if (res.statusCode == 404) {
                if (res.headers.location) {//301
                    console.log('301 redirecting ' + res.req.path + ' to ' + res.headers.location);
                    addURLtoArray(res.headers.location, url.depth, res.req.path);
                    return false;
                }
                else {
                    console.warn(colors.red.underline("NOT FOUND"));
                }
            }
            else if (res.statusCode = 500) console.error(colors.red.underline("ERROR"));
            else console.info(res.statusCode + "RESPONSE");
            var path = 'http://' + option.host + res.req.path;
            console.log(colors.red('Path: ' + path));
            console.log(colors.red('Source: ' + url.source));
            console.log(colors.red('Found ' + url.depth + ' links deep from ' + options.path));
            console.log();
        }
        var data = '';
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            pendingRequests--;
            //console.log(data);
            count = count + 1;
            if ((count / alertEvery) % 1 === 0 || count == urls.length) {
                // console.log('#' + count + ' Depth: '+ url.depth + ' Url: ' + url.url + ' Source: ' + url.source);
                console.log('Processed: ' + count + '/' + urls.length);
            }
            var depth = url.depth;
            if (depth + 1 >= maxdepth) return;
            // var end = new Date().getTime();
            // var time = end - start;
            // if (time > alertAfter){
            // 	console.log(option.path +' Execution time: ' + time);
            // }
            var $ = cheerio.load(data);
            if ($('form').first().attr('action') == '500.aspx?error=true') {
                console.error(colors.red.underline("ERROR"));
                var path = 'http://' + option.host + res.req.path;
                console.log(colors.red('Path:' + path));
                console.log(colors.red('Source:' + url.source));
                console.log(colors.red('Found ' + url.depth + ' links deep from ' + options.path));
                console.log();
            }
            $('a').map(function (i, link) {
                var href = $(link).attr('href');
                addURLtoArray(href, depth, option.path);
            });
        });
    });
    request.on('error', function (e) {
        console.log(colors.red.underline('ERROR! ' + e.message));
    });
    request.end();
}

function addURLtoArray(href, depth, source) {
    if (href && href.length > 0 && (href.indexOf("http://" + options.host) == 0 || href[0] == '/')) {
        var url = {
            hit: false, 
            url: href.toLowerCase(),
            depth: depth + 1,
            source: source
        };
        if (!_.findWhere(urls, { url: url.url })) {
            urls.push(url);
            makeRequest({
                host: options.host, 
                path: url.url
            }, url);
        }
    }
}
 
 */