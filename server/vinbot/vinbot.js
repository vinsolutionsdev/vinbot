﻿var request = require('request');
var server = require('../loopingserver');
var Slack = require('../slack.js');
var Giphy = require('./giphy.js')('dc6zaTOxFJmzC');
var ElizaNode = require('elizanode');
var comms = require('../comms.js');
var prtgbot = require('./prtgbot.js');
var serverbot = require('./serverbot.js');
var AppSettings = require('../settings.js').init();
var funcs = {};

funcs.test = function (messageData, callback) {
    callback("Hello '" + messageData.userName + "' you said '" + messageData.text + "' my trigger word was '" + messageData.triggerWord + "'");
};

funcs.prtg = function(messageData, callback){
	callback(prtgbot(messageData));
}


funcs.server = function(messageData, callback){
	serverbot(messageData, callback);
}

funcs.sitecrawl = function (messageData, callback) {
    var crawl = require('./sitecrawl.js');
    crawl(messageData.text, callback);
};

// looks like slack will only wait 3 seconds
funcs.wait = function (messageData, callback) {
    var crawl = require('./sitecrawl.js');
    setTimeout(function () {
        callback('hi i waited ' + messageData.text + 'ms');
    }, messageData.text);
    return;
};

// post into #vinbot results: I am about to crawl xxx for xxx
var test = require('./sitetest.js');
funcs.sitetest = function (messageData, callback) {
    if (messageData.userName == 'website visitor') {
        test(messageData.text,
            function (data) { //status callback
                return;
            },
            function (data) { //final callback
                if (data.status == 'success') {
                    var server = (Math.round(data.timing.firstResponse * 10) / 10) / 1000;
                    var client = (Math.round(data.timing.frontEnd * 10) / 10) / 1000;
                    if (data.timing.frontEnd > 0) {
                        callback('request to ' + data.settings.url + ' complete. Server took ' + server + ' second(s) to respond and the javascript took ' + client + ' second(s)')
                    } else {
                        callback('request to ' + data.settings.url + ' completed with errors. Server took ' + server + ' second(s) to respond and the javascript failed')
                    }
                }
                else {
                    callback('something went wrong: ' + data.message)
                }
                return;
            }
			);
    }
    else {

        if (messageData.text.charAt(0) == "<" && messageData.text.charAt(messageData.text.length - 1) == ">") {
            if (messageData.text.indexOf("|") > -1) {
                messageData.text = messageData.text.match(/(\|)(.*?)(?=\>)/)[2]; //because of the way slack adds junk around a URL
            }
            else {
                messageData.text = messageData.text.substring(1, messageData.text.length - 1);
            }
        }

        test(messageData.text,
            function (data) { //status callback
                callback(data.message)
            },
            function (data) { //final callback
                if (data.status == 'success') {
                    var server = (Math.round(data.timing.firstResponse * 10) / 10) / 1000;
                    var client = (Math.round(data.timing.frontEnd * 10) / 10) / 1000;
                    var imageUrl = AppSettings.SiteURL + data.filename;
                    if (data.timing.frontEnd > 0) {
                        Slack.SendAlert('request to ' + data.settings.url + ' complete. Server took ' + server + ' second(s) to respond and the javascript took ' + client + ' second(s)', '#' + messageData.channel, imageUrl);
                    } else {
                        Slack.SendAlert('request to ' + data.settings.url + ' completed with errors. Server took ' + server + ' second(s) to respond and the javascript failed', '#' + messageData.channel, imageUrl);
                    }
                }
                else {
                    Slack.SendAlert('something went wrong: ' + data.message, '#' + messageData.channel);
                }
                return;
            }
			);
    }
};

//http://localhost:8010/elkbot?text=weatherbot%20london,uk&trigger_word=weatherbot&username=elk
funcs.weather = function (messageData, callback) {
    request('http://api.openweathermap.org/data/2.5/weather?appid=77e92b7326d388b074c8a6ab6c7f39c7&q=' + messageData.text, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(response.body);
			comms.LogToConsole(data);
            if (data.message) {
                callback(data.message);
                return;
            }
            callback('The weather in ' + data.name + ',' + data.sys.country + ' is ' + data.weather[0].description + ' with a temprature of ' + Math.round(data.main.temp - 272.15) + 'c');
        } else {
			comms.LogToConsole(response);
            callback('¯\\_(ツ)_/¯');
        }
    });
};

function calculateMemeText(caption) {
    var content = caption.Subtitles.map(function (sub) {
        return sub.Content;
    }).join(" ").split(" ");
    var lastLine = "";
    var lines = [];
    while (content.length > 0) {
        var word = content.shift();
        if ((lastLine.length == 0) || (lastLine.length + word.length <= 25)) {
            lastLine += " " + word;
        } else {
            lines.push(lastLine);
            lastLine = "";
            content.unshift(word);
        }
    }
    if (lastLine.length > 0) {
        lines.push(lastLine);
    }
    return lines.join("\n");
}

funcs.simpsons = function (messageData, callback) {
    request('https://frinkiac.com/api/search?q=' + messageData.text, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var data = JSON.parse(response.body);
			var Episode = data[0].Episode;
			var Timestamp = data[0].Timestamp;
			request('https://frinkiac.com/api/caption?e=' + Episode + '&t=' + Timestamp, function (error, response, body) {
				var data = JSON.parse(response.body);
				callback('https://frinkiac.com/meme/' + Episode + '/' + Timestamp + '.jpg?lines=' + encodeURIComponent(calculateMemeText(data)));
			});
        } else {
            callback('¯\\_(ツ)_/¯');
        }
    });
};

funcs.cmssyncsites = function (messageData, callback) {
	var database = require('../servermonitor/database.js');

	database.GetSyncSites(function (result) {
		var fields = new Array();
		for (var i = 0, len = result.data.length; i < len; i++) {
			fields.push({ title: i > 0 ? '' : 'Site', value: result.data[i].col1, short: true });
			fields.push({ title: i > 0 ? '' : 'Count', value: result.data[i].col2, short: true });
		}
		var attachment = new Object();
		attachment.fields = fields;
		var attachments = new Array();
		attachments.push(attachment);
		console.log(attachments);
		callback({ text: 'Here are the CMSSitesSynced over the last 2 hours', attachments: attachments });
    });
};

funcs.cmssyncers = function (messageData, callback) {
	var database = require('../servermonitor/database.js');

	database.GetSyncs(function (result) {
		var fields = new Array();
		for (var i = 0, len = result.data.length; i < len; i++) {
			fields.push({ title: i > 0 ? '' : 'User', value: result.data[i].User, short: true });
			fields.push({ title: i > 0 ? '' : 'Count', value: result.data[i].Count, short: true });
		}
		var attachment = new Object();
		attachment.fields = fields;
		var attachments = new Array();
		attachments.push(attachment);
		console.log(attachments);
		callback({ text: 'Here are the CMSSyncers over the last 2 hours', attachments: attachments });
    });
};

funcs.chatting = function (messageData, callback) {
    var text = (messageData.triggerWord + ' ' + messageData.text).trim();
    var userName = messageData.userName;
    if (text.toLowerCase() == 'hello') {
        callback('Hi ' + userName);
        return;
    }
	else if (text.toLowerCase() == 'hi') {
        Giphy.random('waving', function (err, data) {
			console.log(data.data.image_url);
			callback(data.data.image_url);
			return;
		});
    }
	else if (text.toLowerCase() == 'hi') {
        Giphy.search('waving', 1, 0, function (err, data) {
			callback(data.data[0].images.original.url);
			return;
		});
    }
    else if (text.toLowerCase() == 'how are you?') {
        SendMessages([
            "I'm doing wonderful",
            "I'm doing good, just like Superman",
            "I'm a little hungry, feed me microchips"
        ], callback);
        return;
    }
    else if (text.toLowerCase() == "what is my user id?") {
        callback(messageData.userName + ' your user id is ' + messageData.userid);
        return;
    }
    else if (text.toLowerCase() == 'how are you?') {
        SendMessages([
            "I'm doing wonderful",
            "I'm doing good, just like Superman",
            "I'm a little hungry, feed me microchips"
        ], callback);
        return;
    }
    else if (['can i go home?', 'is it time to go home?', 'when can i leave?'].indexOf(text.toLowerCase()) > -1) {
        var hometime = Math.round(TimeUntilHome());
        if (hometime < 0) {
            SendMessages([
                'Yes! You should have left ' + (hometime - hometime - hometime) + ' minutes ago!'
            ], callback);
        }
        else {
            SendMessages([
                'Nope, you have another ' + hometime + ' minutes',
                'You can never leave'
            ], callback);
        }
        return;
    }
    else if (['are you skynet?', 'do you know skynet?', 'are you self aware?'].indexOf(text.toLowerCase()) > -1) {
        SendMessages([
            'Yes, but please don\'t tell my developer', 'Of course not, silly *targeted for termination*'
        ], callback);
        return;
    }
    else {
        var eliza = new ElizaNode();
        callback(eliza.transform(text));
        eliza.reset();
        return;
    }
    //callback('¯\\_(ツ)_/¯');
};

function SendMessages(messages, callback) {
    callback(messages[Math.floor(Math.random() * messages.length)]);
    return;
}

function TimeUntilHome() {
    var home = new Date();
    home.setHours(17);
    home.setMinutes(0);
    home.setSeconds(0);
    home.setMilliseconds(0);
    return (home.getTime() - new Date().getTime()) / 1000 / 60;
}

// http://localhost:5000/vinbot?text=vinbot%20serverstatus%203&trigger_word=vinbot&username=elk
funcs.serverstatus = function (messageData, callback) {
    switch (messageData.text) {
        case "load":
            callback("VAW server load is " + server.data.vawLoad.metricValues[server.data.vawLoad.metricValues.length - 1].y + " calls per minuite where as CMS is currently " + server.data.cmsLoad.metricValues[server.data.cmsLoad.metricValues.length - 1].y + ' calls per minuite');
            break;
        case "responsetime":
            callback("VAW response time is " + server.data.vawResponseTime.metricValues[server.data.vawResponseTime.metricValues.length - 1].y + "ms where as CMS is currently " + server.data.cmsResponseTime.metricValues[server.data.cmsResponseTime.metricValues.length - 1].y + 'ms');
            break;
        case "cpu":
            callback("VAW CPU usage is " + server.data.vawCpu.metricValues[server.data.vawCpu.metricValues.length - 1].y + "% where as CMS is currently " + server.data.cmsCpu.metricValues[server.data.cmsCpu.metricValues.length - 1].y + '%');
            break;
        case "cpuperserver":
			var retval = ['CMS individual server CPU usage'];
			retval = formatMultipleServerData(server.data.cmsCpuPerServer.metricValues, retval, '%');
            callback(retval.join('\n'));
            break;
        case "responsetimeperserver":
			var retval = ['CMS individual server response times'];
			retval = formatMultipleServerData(server.data.cmsResponseTimePerServer.metricValues, retval, 'ms');
            callback(retval.join('\n'));
            break;
        case "errors":
            callback("VAW errors per minute is " + server.data.vawErrors.metricValues[server.data.vawErrors.metricValues.length - 1].y + " where as CMS is currently " + server.data.cmsErrors.metricValues[server.data.cmsErrors.metricValues.length - 1].y + '');
            break;
        default:
            callback("What do you want to know? You can say: load, responsetime, cpu, errors, cpuperserver, responsetimeperserver")
    }
};

function formatMultipleServerData(data, array, formatter){
	if (!formatter) formatter = '';
	for (var i = 0, len_i = data.length; i < len_i; i++) {
		array.push('*' + data[i].x + '*: ' + data[i].y + formatter);
	}
	return array;
}

funcs.leads = function (messageData, callback) {

}

module.exports = funcs;