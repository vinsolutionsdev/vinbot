var cp = require('child_process');
var AppSettings = require('../settings.js').init();
var fs = require('fs');

module.exports.ResetIISOnServer = function (serverName, callback) {
	//	ExecuteCommand('iisreset', [serverName],callback);
	//return;
	if (!HeyIsThisServerGood(serverName)) {
		callback("invalid server: " + serverName);
		return;
	}
	ExecuteCommand('iisreset', [serverName], function (err, output) {
		if (err) {
			callback(err);
		} else {
			callback(output);
		}
	});
}

module.exports.PutInLoad = function (serverName, callback) {
	if (!HeyIsThisServerGood(serverName)) {
		callback("invalid server: " + serverName);
		return;
	}
	var path = "\\\\" + serverName + '\\e$\\VinSolutions\\WebApps\\DealerSitesApp\\'
	fs.rename(path + 'gsi_default.htm.bak', path + 'gsi_default.htm', function (err) {
		if (err) { callback(err); return; }
		callback(serverName + ' Put in load');
	});
}

module.exports.RemoveFromLoad = function (serverName, callback) {
	if (!HeyIsThisServerGood(serverName)) {
		callback("invalid server: " + serverName);
		return;
	}
	var path = "\\\\" + serverName + '\\e$\\VinSolutions\\WebApps\\DealerSitesApp\\'
	fs.rename(path + 'gsi_default.htm', path + 'gsi_default.htm.bak', function (err) {
		if (err) { console.log(err); callback(err); return; }
		callback(serverName + ' Removed from load');
	});
}

function HeyIsThisServerGood(servername) {
	return AppSettings.SafeServers.indexOf(servername) >= 0
}

function ExecuteCommand(command, args, callback) {
	console.log(command);
	console.log(args);
    var spawn = cp.spawn(command, args);
    var output = "";
	spawn.on('error', function (err) {
		console.log('error');
		console.log(err);
		callback(err, null);
	});
	spawn.stdout.on('data', function (data) {
		output += String(data);
	});
	spawn.on('exit', function (code) {
		if (code === 0) {
			callback(null, output);
		} else {
			callback(Error(output), null);
		}
	});
}