
//tiny socketio wrapper for sending messages to the client
io = require('socket.io');
var AppSettings = require('./settings.js').init();
var request = require('request');
var io;
exports.initcomms = function(server) {
	console.log('hailing frequencies open');
  io = require('socket.io').listen(server);
  io.on('connection', function (socket) {
    socket.emit('warmup', { value: 'Connected!' });
  });
};
exports.sendMessage = function(message) {
	console.log(message);
	io.sockets.emit('warmup', { value: message });
};

exports.sendData = function(data) {
  io.sockets.emit('dataupdate', { value: data });
};

exports.sendResult = function(data) {
  io.sockets.emit('dataresult', { value: data });
};

exports.LogToConsole = function(message) {
  if (!AppSettings.Debugging) return;
  console.log(message);
}

var maxConnections = 25;
var currentConnections = 0;
exports.RequestData = function (uriObject, callback) {
    if (currentConnections > maxConnections) {
        //exports.LogToConsole("connection requested, but: " + currentConnections);
        setTimeout(function () {
            exports.RequestData(uriObject, callback);
        }, Math.floor(Math.random() * 2) + 250); //try again between half a second and 2 seconds
    }
    else {
        currentConnections++;
        //exports.LogToConsole("connection requested, now: " + currentConnections);
        request(uriObject, function (error, response, body) {
            //exports.LogToConsole("connection completed, now: "+ currentConnections);
            currentConnections--;
            callback(error, response, body);
        });
    }
}