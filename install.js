var Service = require('node-windows').Service;

// Create a new service object
var svc = new Service({
  name:'_NodeJS Server Monitor',
  description: 'Monitoring.',
  script: 'E:\\VinSolutions\\Node\\ServerMonitor\\app.js'
});

// Listen for the "install" event, which indicates the
// process is available as a service.
svc.on('install',function(){
	console.log('installing ');
  svc.start();
	console.log('we started');
});
svc.on('error',function(){
	console.log('error');
});
svc.on('alreadyinstalled',function(msg){
	console.log('alreadyinstalled ' + svc.exists);
});
svc.on('invalidinstallation',function(){
	console.log('invalidinstallation');
});
svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

console.log('svc is');
console.log(svc);

svc.uninstall();
svc.install();