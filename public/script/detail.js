'use strict';
angular.module('VINServerMonitor.detail', ['ngRoute'])
    .controller('DetailCtrl', ['$scope', '$http', '$templateCache',
        function ($scope, $location, $anchorScroll) {
            $scope.scrollTo = function (id) {
                var old = $location.hash();
                $location.hash(id);
                $anchorScroll();
                //reset to old to keep any additional routing logic from kicking in
                $location.hash(old);
            };

            var vawchart = new CanvasJS.Chart('vaw');
            var cmschart = new CanvasJS.Chart('cms');
            var vawloadchart = new CanvasJS.Chart('vawload');
            var cmsloadchart = new CanvasJS.Chart('cmsload');
            var vawcpuchart = new CanvasJS.Chart('vawcpu');
            var cmscpuchart = new CanvasJS.Chart('cmscpu');
            var vawbuisnesschart = new CanvasJS.Chart('vawbuisness');
            var cmsbuisnesschart = new CanvasJS.Chart('cmsbuisness');
            var cmserrorschart = new CanvasJS.Chart('cmserrors');
            var vawerrorschart = new CanvasJS.Chart('vawerrors');
            var leadschart = new CanvasJS.Chart('leads');
            var unimportedleadschart = new CanvasJS.Chart('unimportedleads');

            var responseTimeWarning = new Warnings();
            responseTimeWarning.warningLevel = 250;
            responseTimeWarning.criticalLevel = 500;

            var leadWarning = new Warnings();
            leadWarning.warningLevel = 2;
            leadWarning.criticalLevel = 1;
            leadWarning.upperCheck = false;

            GetData();

            function GetData() {

                $.ajax({
                    url: "/getData"
                }).done(function (data) {
                    console.log(data);

                    if (data.badLeadsData != undefined) {
                        $('#badleads').html('<strong>' + data.badLeadsData[0].metricValues + '</strong> Failing Leads in the last week');
                        $('#ignoredleads').html('<strong>' + data.badLeadsData[1].metricValues + '</strong> Ignored Leads in the last week');
                    }
                    if (data.cmsBuisnessTransactions != undefined) {
                        AddMultiLineChart(cmsbuisnesschart, data.cmsBuisnessTransactions, { title: "CMS Buisness Transactions Response Time", miny: 0 });
                    }
                    if (data.vawBuisnessTransactions != undefined) {
                        AddMultiLineChart(vawbuisnesschart, data.vawBuisnessTransactions, { title: "VAW Buisness Transactions Response Time", miny: 0 });
                    }
                    if (data.cmsLoad != undefined && data.cmsLoad.metricValues != undefined) {
                        data.cmsLoad.maxy = 5000;
                        AddLineChart(cmsloadchart, data.cmsLoad);
                    }

                    if (data.vawCpu != undefined && data.vawCpu.metricValues != undefined) {
                        data.vawCpu.maxy = 100;
                        AddLineChart(vawcpuchart, data.vawCpu);
                    }
                    if (data.vawErrors != undefined && data.vawErrors.metricValues != undefined) {
                        data.vawErrors.maxy = 1000;
                        AddLineChart(vawerrorschart, data.vawErrors);
                    }

                    if (data.cmsCpu != undefined && data.cmsCpu.metricValues != undefined) {
                        data.cmsCpu.maxy = 100;
                        AddLineChart(cmscpuchart, data.cmsCpu);
                    }
                    if (data.cmsErrors != undefined && data.cmsErrors.metricValues != undefined) {
                        data.cmsErrors.maxy = 50;
                        AddLineChart(cmserrorschart, data.cmsErrors);
                    }
                    if (data.cmsResponseTime != undefined && data.cmsResponseTime.metricValues != undefined) {
                        data.cmsResponseTime.maxy = 750;
                        AddLineChart(cmschart, data.cmsResponseTime);
                        responseTimeWarning.check(data.cmsResponseTime, 'cms', cmschart);
                    }

                    if (data.cmsStatus != undefined && data.cmsStatus.metricValues != undefined) {
                        AddStatusButtons(data.cmsStatus, $('#cmsStatus'));
                    }


                    if (data.leadsData != undefined && data.leadsData[0].metricValues != undefined) {
                        AddLeadsChart(leadschart, 'goodleads', [data.leadsData[1], data.leadsData[3], data.leadsData[5], data.leadsData[7]]);
                        AddLeadsChart(unimportedleadschart, 'badleads', [data.leadsData[2], data.leadsData[4], data.leadsData[6], data.leadsData[8]]);
                    }

                    if (data.vawLoad != undefined && data.vawLoad.metricValues != undefined) {
                        data.vawLoad.maxy = 30000;
                        AddLineChart(vawloadchart, data.vawLoad);
                    }


                    if (data.vawResponseTime != undefined && data.vawResponseTime.metricValues != undefined) {
                        data.vawResponseTime.maxy = 750;
                        AddLineChart(vawchart, data.vawResponseTime);
                        responseTimeWarning.check(data.vawResponseTime, 'vaw', vawchart);
                    }

                    if (data.vawStatus != undefined && data.vawStatus.metricValues != undefined) {
                        AddStatusButtons(data.vawStatus, $('#vawStatus'));
                    }
                    if (data.vawServerStatus != undefined && data.vawStatus.metricValues != undefined) {
                        AddServerStatusButtons(data.vawServerStatus, $('#vawServerStatus'));
                    }

                    if (data.replicaitonStatus != undefined && data.replicaitonStatus.metricValues != undefined) {
                        AddReplicationStatus(data.replicaitonStatus, $('#replicationStatus'));
                    }
                });
                setTimeout(function () {
                    GetData();
                }, 60000);
            }
            function AddReplicationStatus(data, div) {
                div.find('div').remove();
                for (var i = 0; i < data.metricValues.length; i++) {
                    var item = data.metricValues[i];
                    div.append('<div>'+item.Publication+': '+item.Latency+' second(s)</div>')
                }
            }
            function AddServerStatusButtons(data, div) {
                div.find('.btn').remove();
                for (var i = 0; i < data.metricValues.length; i++) {
                    var server = data.metricValues[i];
                    var cssicon = '';
                    var cssclass = '';
                    if (server.Okay == false) {
                        cssclass = 'danger';
                        cssicon = 'remove-sign';
                    }
                    else if (server.Okay == true) {
                        cssclass = 'success';
                        cssicon = 'ok-sign';
                    }
                    div.append('<div class="btn-' + cssclass + ' btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="' + server.Server + '"><span class="glyphicon glyphicon-' + cssicon + '" aria-hidden="true"></span></div>');
                    div.find('[data-toggle="tooltip"]').tooltip();
                }
            }
            function AddStatusButtons(data, div) {
                div.find('.btn').remove() ;
                for (var i = 0; i < data.metricValues.length; i++) {
                    var server = data.metricValues[i];
                    var cssicon = '';
                    var cssclass = '';
                    if (server.status == 'Down') {
                        cssclass = 'danger';
                        cssicon = 'remove-sign';
                    }
                    else if (server.status == 'Up') {
                        cssclass = 'success';
                        cssicon = 'ok-sign';
                    }
                    else {
                        cssclass = 'warning';
                        cssicon = 'question-sign';
                    }
                    div.append('<div class="btn-' +cssclass + ' btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="' + server.device + ' - ' +server.status + '"><span class="glyphicon glyphicon-' +cssicon + '" aria-hidden="true"></span></div>');
                    div.find('[data-toggle="tooltip"]').tooltip();
                }
            }

            function Warnings() {
                this.upperCheck = true;
                this.warningLevel = 0;
                this.criticalLevel = 0;
                this.occurancesToScan = 3;
                this.check = function (data, div, chart) {
                    var criticalCount = 0;
                    var warningCount = 0;
                    var critical = true;
                    var warning = true;
                    var criticalCount = 0;
                    var warningCount = 0;
                    for (var i = data.metricValues.length -1; i > 0; i--) {
                        if (!critical && !warning) break;
                        var tempdata = data.metricValues[i];
                        if (critical) {
                            if ((this.upperCheck && (tempdata.y > this.criticalLevel)) || (!this.upperCheck && (tempdata.y < this.criticalLevel))) {
                                criticalCount++;
                            }
                            else { //if (data.metricValues.length - i >= this.occurancesToScan) {
                                critical = false;
                        }
                    }
                        if (warning) {
                            if ((this.upperCheck && (tempdata.y > this.warningLevel)) || (!this.upperCheck && (tempdata.y < this.warningLevel))) {
                                warningCount++;
                            }
                            else { //if (data.metricValues.length - i >= this.occurancesToScan) {
                                warning = false;
                        }
                    }
                }
                    //console.log(div + ' warningCount: ' + warningCount + ' | criticalCount: ' + criticalCount);
                    $('#' +div).parent().find('.alert').remove();
                    if (criticalCount >= this.occurancesToScan) {
                        $('#' +div).after('<div class="alert alert-danger" role="alert">DANGER! Value: ' +data.metricValues[data.metricValues.length -1].y + ' is ' +(this.upperCheck ? 'over': 'under') + ' the threshhold of ' +this.criticalLevel + '.<br/>Alert has been observed for ' +criticalCount + ' occurances</div');
                        //chart.options.backgroundColor = "#f2dede";
                    }
                    else if (warningCount >= this.occurancesToScan) {
                        $('#' +div).after('<div class="alert alert-warning" role="alert">WARNING! Value: ' +data.metricValues[data.metricValues.length -1].y + ' is ' +(this.upperCheck ? 'over': 'under') + ' the threshhold of ' +this.warningLevel + '.<br/>Alert has been observed for ' +warningCount + ' occurances</div');
                        //chart.options.backgroundColor = "#fcf8e3";
                    }
                    else {
                        //chart.options.backgroundColor = "#dff0d8";
                    }
                    if (this.upperCheck) {
                        chart.options.axisY.stripLines =[{
                            startValue: this.warningLevel,
                            endValue: this.criticalLevel,
                            color: "#fcf8e3"
                        }, {
                            startValue: this.criticalLevel,
                            endValue: 9999999,
                            color: "#f2dede"
                    }];
                    }
                    else {
                        chart.options.axisY.stripLines =[{
                            startValue: this.warningLevel,
                            endValue: this.criticalLevel,
                            color: "#fcf8e3"
                        }, {
                            startValue: this.criticalLevel,
                            endValue: -9999999,
                            color: "#f2dede"
                    }];
                    }
                    chart.render();
            };
            }
            function AddMultiLineChart(chart, data, options) {
                chart.options.theme = "theme3";
                chart.options.backgroundColor = 'white';
                chart.options.title = {
                    text:options.title
                };
                chart.options.zoomEnabled = true;
                //chart.options.animationEnabled = true;
                chart.options.axisX = {
                    valueFormatString: "D/M/Y H:mm",
                    intervalType: "minute",
                    labelFontSize: 15
                };
                chart.options.axisY = {
                    maximum: options.maxy,
                    minimum: options.miny
                };
                chart.options.data = new Array();
                for (var i = 0; i < data.length; i++) {
                    chart.options.data.push({
                        xValueType: "dateTime",
                        type: "spline", dataPoints: data[i].metricValues,
                        toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                        lineThickness: 2,
                        name: data[i].Path
                    });
                }
                chart.render();
            }
            function AddLineChart(chart, data) {
                chart.options.theme = "theme3";
                chart.options.backgroundColor = 'white';
                chart.options.title = {
                    text: data.id
            };
                //chart.options.zoomEnabled = true;
                chart.options.subtitles =[{
                    text: data.Path
            }];
                //chart.options.animationEnabled = true;
                chart.options.axisX = {
                    valueFormatString: "D/M/Y H:mm",
                    intervalType: "minute"
                };
                chart.options.axisY = {
                    includeZero: false,
                    maximum: data.maxy,
                    minimum: 0
                };
                chart.options.data =[{
                    xValueType: "dateTime",
                    type: "line",
                        toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                    lineThickness: 3,
                    name: data.Path,
                    dataPoints: data.metricValues
                //showInLegend: true
                }];
                chart.render();
            }

            function AddLeadsChart(chart, id, data) {
                chart.options.theme = "theme3";
                chart.options.title = {
                    text: id
                };
                chart.options.backgroundColor = 'white';
                //chart.options.zoomEnabled = true;
                //chart.options.animationEnabled = true;
                chart.options.axisX = {
                    valueFormatString: "D/M/Y H:mm",
                    intervalType: "minute"
                };
                chart.options.axisY = {
                    includeZero: false
                };
                chart.options.data =[{
                    xValueType: "dateTime",
                    type: "line",
                        toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                    lineThickness: 3,
                    name: data[0].Path,
                    dataPoints: data[0].metricValues,
                    showInLegend: true
                    }, {
                        xValueType: "dateTime",
                        type: "line",
                            toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                        lineThickness: 3,
                        name: data[1].Path,
                        dataPoints: data[1].metricValues,
                        showInLegend: true
                        }, {
                            xValueType: "dateTime",
                            type: "line",
                                toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                            lineThickness: 3,
                            name: data[2].Path,
                            dataPoints: data[2].metricValues,
                            showInLegend: true
                            }, {
                                xValueType: "dateTime",
                                type: "line",
                                    toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
                                lineThickness: 3,
                                name: data[3].Path,
                                dataPoints: data[3].metricValues,
                                showInLegend: true
                }];
                chart.render();
            }
            $(document).ready(function () {
                $("body").toasty({ image: "/script/toasty-master/toasty.png", sound: "/script/toasty-master/toasty.mp3"
                });
                var easter_egg = new Konami(function() { $("body").toasty('pop')
            });
            });
        }
    ]);