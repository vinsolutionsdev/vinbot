var Base64 = require('./base64.js');
var comms = require('../comms.js');
var AppSettings = require('../settings.js').init();
//var applicationPath = 'https://vinsolutions.saas.appdynamics.com/controller/rest/applications/Websites%201.0_2.0%20-%20Production/metric-data';
//var defaultTimeRange = '&time-range-type=BEFORE_NOW&duration-in-mins=15';
var EncodedAuth = Base64.encode(AppSettings.AppDynamics.Auth.login + "@" + AppSettings.AppDynamics.Auth.account + ":" + AppSettings.AppDynamics.Auth.password);
var request = require('request');

function HandleSuccessfulRequest(error, response, body, callback) {
	if (!error && response.statusCode == 200) {
		if (JSON.parse(response.body).length > 1) {
			FormatMultipleServers(JSON.parse(response.body), callback);
		}
		else {
			FormatSingleServer(JSON.parse(response.body)[0], callback);
		}
	} else {
		comms.LogToConsole(error);
	}
}

function FormatMultipleServers(data, callback) {
	var retval = { id: 'appDynamics_' + data[0].metricId, metricValues: new Array() };
	//var retval = { id: 'appDynamics_' + data.metricId, Path: data.metricPath, metricValues: new Array() };
	for (var i = 0, len_i = data.length; i < len_i; i++) {
		var metricValue = { x: data[i].metricPath }
		for (var j = 0, len_j = data[i].metricValues.length; j < len_j; j++) {
			metricValue.y = data[i].metricValues[j].value;
		}
		if (metricValue.y > 0) {
			retval.metricValues.push(metricValue);
		}
	}
	callback(FindPathDifference(retval));
}

function FindPathDifference(data) {
	if (data.metricValues.length == 0) {
		return data;
	}
	var metricItems = [];
	for (var i = 0, len = data.metricValues.length; i < len; i++) {
		metricItems.push(data.metricValues[i].x.split('|'));
	}
	var same = [];
	for (var i = 0, len = metricItems[0].length; i < len; i++) {
		if (metricItems[0][i] == metricItems[1][i]) {
			same.push(metricItems[0][i]);
		}
	}
	for (var i = 0, len = data.metricValues.length; i < len; i++) {
		for (var j = 0, j_len = same.length; j < j_len; j++) {
			data.metricValues[i].x = data.metricValues[i].x.replace(same[j], '');
		}
		data.metricValues[i].x = data.metricValues[i].x.replace(/\|/g, '');


	}
	data.metricValues.sort(pathsorter);
	data.x = same.join('|');
	return data;
}

function pathsorter(a, b) {
	if (a.x < b.x)
		return -1;
	else if (a.x > b.x)
		return 1;
	else
		return 0;
}

function FormatSingleServer(data, callback) {
	var retval = { id: 'appDynamics_' + data.metricId, Path: data.metricPath, metricValues: new Array() };
	for (var i = 0, len = data.metricValues.length; i < len; i++) {
		var meticValues = data.metricValues[i];
		var time = new Date(meticValues.startTimeInMillis);
		//var metricTime = time.getDate() + '/' + ('0'+(time.getMonth()+1)).slice(-2) + ' ' + time.getHours() + ':' + (time.getMinutes()<10?'0':'') + time.getMinutes(); 
		if (meticValues.startTimeInMillis > 1438876247000 && data.metricId == 'fsfsf' && retval.Path == "Overall Application Performance|Average Response Time (ms)") { meticValues.value = meticValues.value * 100; } //get out of retros
		retval.metricValues.push({ y: meticValues.value, x: meticValues.startTimeInMillis });
	}
	callback(retval);
}

module.exports.getAppDynamics = function (mins, application, metricpath, callback) {
	var url = 'https://vinsolutions.saas.appdynamics.com/controller/rest/applications/' + application + '/metric-data?metric-path=' + metricpath + '&time-range-type=BEFORE_NOW&duration-in-mins=' + mins + '&rollup=false&output=JSON';
	comms.LogToConsole('requesting: ' + url);
	comms.RequestData({ url: url, headers: { "Authorization": "Basic " + EncodedAuth } }, function (error, response, body) {
		HandleSuccessfulRequest(error, response, body, function (result) {
			callback(result);
		});
	});
}

module.exports.getMetricItems = function (application, metricpath, callback) {
    var url = 'https://vinsolutions.saas.appdynamics.com/controller/rest/applications/' + application + '/metrics?metric-path=' + metricpath + '&output=JSON';
    comms.LogToConsole('requesting: ' + url);
    comms.RequestData({ url: url, headers: { "Authorization": "Basic " + EncodedAuth } }, function (error, response, body) {
        if (error) {
            console.log(error);
            return;
        }
        try {
            var data = JSON.parse(response.body);
            var metricItems = new Array();
            for (var i = 0, len = data.length; i < len; i++) {
                metricItems.push(data[i].name);
            }
            callback(metricItems);
        }
        catch (ex) {
            return;
        }
    });
}


//module.exports.getVAWData = function (mins, path, callback){
//	var url = 'https://vinsolutions.saas.appdynamics.com/controller/rest/applications/'+path+'&time-range-type=BEFORE_NOW&duration-in-mins='+mins+'&rollup=false&output=JSON';
//	comms.LogToConsole('requesting: ' + url);
//	request({url : url, headers : { "Authorization" : "Basic " +EncodedAuth} }, function (error, response, body) {
//		HandleSuccessfulRequest(error, response, body, function(result){
//			callback(result);
//		});
//	});
//}