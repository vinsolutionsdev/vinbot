var express = require('express');
var app = express.Router();


var vinbot = require('./vinbot/routes.js');
app.use('/', vinbot);
var servermonitor = require('./servermonitor/routes.js');
app.use('/', servermonitor);
var api = require('./api/routes.js');
app.use('/api', api);

module.exports = app;