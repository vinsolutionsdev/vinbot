module.exports.init = function (){
    try {
        var override = require('../config/enviromentSettings.js');
        var data = require('../config/defaultSettings.js');
        var obj = {};
        
        for (var x in data)
            if (data.hasOwnProperty(x))
                obj[x] = data[x];
        
        for (var x in override)
            if (override.hasOwnProperty(x))
                obj[x] = override[x];
        
        return obj;
    }
    catch(err) {
        return require('../config/defaultSettings.js');
    }
}