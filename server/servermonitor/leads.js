
var request = require('request');
var comms = require('../comms.js');
var AppSettings = require('../settings.js').init();
module.exports.getLeadsData = function (callback){
	//var url = 'http://wsvc.qa.vinsolutions.com/MonitoringService/Leads/GetAllLeadCounts';
	var url = AppSettings.ServerMonitorURL + 'Leads/GetAllLeadCounts';
	comms.LogToConsole('requesting: ' + url);
    comms.RequestData({url : url}, function (error, response, body) {
        HandleSuccessfulGetLeadsDataRequest(error, response, body, function(result){
			callback(result);
		});
	});
}

module.exports.getBadLeadsData = function (callback) {
    //var url = 'http://wsvc.qa.vinsolutions.com/MonitoringService/Leads/GetAllLeadCounts';
    var url = AppSettings.ServerMonitorURL + 'Leads/GetAllFailingLeadCounts';
    comms.LogToConsole('requesting: ' + url);
    comms.RequestData({ url : url }, function (error, response, body) {
        HandleSuccessfulGetBadLeadsDataRequest(error, response, body, function (result) {
            callback(result);
        });
    });
}

function HandleSuccessfulGetBadLeadsDataRequest(error, response, body, callback) {
    if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        var retval = new Array();
        retval.push({ id: 'FailingLeads', metricValues: data[0] })
        retval.push({ id: 'IgnoredLeads', metricValues: data[1] })
        callback(retval);
        return;
    } else {
        comms.LogToConsole('error');
        comms.LogToConsole(error);
        comms.LogToConsole('response');
        comms.LogToConsole(response);
        comms.LogToConsole('body');
        comms.LogToConsole(body);
    }
}

function HandleSuccessfulGetLeadsDataRequest(error, response, body, callback){
	if (!error && response.statusCode == 200) {
		var data = JSON.parse(body);
		var retval = new Array();
		for (var i = 0, len = data.length; i < len; i++) {
			var meticValues = new Array();
			for (var j = 0, leng = data[i].LeadCounts.length; j < leng; j++) {
				var metric = data[i].LeadCounts[j];
				var time = parseInt(metric.t.replace("/Date(", "").replace(")/",""), 10);
				meticValues.push({ y: metric.c, x: time  });
			}
			retval.push( { id: 'Leads', Path: data[i].Type, metricValues: meticValues })
		}
		callback(retval);
		return;
	} else {
		comms.LogToConsole('error');
		comms.LogToConsole(error);
		comms.LogToConsole('response');
		comms.LogToConsole(response);
		comms.LogToConsole('body');
		comms.LogToConsole(body);
	}
}