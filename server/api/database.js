var sql = require('mssql'),
    fs = require('fs'),
    path = require('path');
var AppSettings = require('../settings.js').init();

module.exports.RunSQL = function (sqlfile, params, callback){
	fs.readFile(path.join(__dirname, '/sql/'+sqlfile+'.sql'), 'utf8', function (err, query) {
		if (err) { 
			console.log(err);
			callback({ status: 'fail', data: err });
			return;
		}
		var connection = new sql.Connection(AppSettings.DatabaseServer, function (err) {
			if (err) {
				console.log(err);
				callback({ status: 'fail', data: err });
				return;
			}
			var request = new sql.Request(connection);
			
			for (var i = 0, len = params.length; i < len; i++) {
				query = query.replace("{0}", params[i]);
			}
			
			
			
			
			
			request.query(query, function (err, recordset) {
				if (typeof recordset == 'undefined' || (err && err.length > 0)){
					callback({ status: 'fail', data: err });
					return;
				}
				callback({ status: 'success', data: recordset });
			});
		});
	});
	return;
}