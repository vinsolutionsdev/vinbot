//setTimeout(function() {
//	window.location.reload(1);
//}, 60000);

//var vawchart = new CanvasJS.Chart('vaw');
//var cmschart = new CanvasJS.Chart('cms');
//var vawloadchart = new CanvasJS.Chart('vawload');
//var cmsloadchart = new CanvasJS.Chart('cmsload');
//var leadschart = new CanvasJS.Chart('leads');
//var unimportedleadschart = new CanvasJS.Chart('unimportedleads');

//var responseTimeWarning = new Warnings();
//responseTimeWarning.warningLevel = 250;
//responseTimeWarning.criticalLevel = 500;

//var leadWarning = new Warnings();
//leadWarning.warningLevel = 2;
//leadWarning.criticalLevel = 1;
//leadWarning.upperCheck = false;

//$.ajax({
//	url: "/getVawResponseTime"
//}).done(function(data) {
//	console.log(data);
//	data.maxy = 750;
//	AddLineChart(vawchart, data);
//	responseTimeWarning.check(data, 'vaw', vawchart);
//});
//$.ajax({
//	url: "/getVawLoad"
//}).done(function(data) {
//	console.log(data);
//	data.maxy = 30000;
//	AddLineChart(vawloadchart, data);
//});
//$.ajax({
//	url: "/getCmsLoad"
//}).done(function(data) {
//	console.log(data);
//	data.maxy = 5000;
//	AddLineChart(cmsloadchart, data);
//});
//$.ajax({
//	url: "/getCmsResponseTime"
//}).done(function(data) {
//	console.log(data);
//	data.maxy = 750;
//	AddLineChart(cmschart, data);
//	responseTimeWarning.check(data, 'cms', cmschart);
//});
//$.ajax({
//	url: "/getleads"
//}).done(function(data) {
//	AddLeadsChart(leadschart, [data[1], data[3], data[5], data[7]]);
//	AddLeadsChart(unimportedleadschart, [data[2], data[4], data[6], data[8]]);
//});
//$.ajax({
//	url: "/getVawStatus"
//}).done(function(data) {
//	console.log(data);
//	AddStatusButtons(data, $('#vawStatus'));
//});
//$.ajax({
//	url: "/getWsvcStatus"
//}).done(function(data) {
//	console.log(data);
//	AddStatusButtons(data, $('#wsvcStatus'));
//});
//$.ajax({
//	url: "/getCmsStatus"
//}).done(function(data) {
//	console.log(data);
//	AddStatusButtons(data, $('#cmsStatus'));
//});

//function AddStatusButtons(data, div) {
//	for (var i = 0; i < data.metricValues.length; i++) {
//		var server = data.metricValues[i];
//		var cssicon = '';
//		var cssclass = '';
//		if (server.s == 'Down') {
//			cssclass = 'danger';
//			cssicon = 'remove-sign';
//		}
//		else if (server.s == 'Up') {
//			cssclass = 'success';
//			cssicon = 'ok-sign';
//		}
//		else {
//			cssclass = 'warning';
//			cssicon = 'question-sign';
//		}
//		div.append('<div class="btn-' + cssclass + ' btn btn-xs" data-toggle="tooltip" data-placement="bottom" title="' + server.d + ' - ' + server.s + '"><span class="glyphicon glyphicon-' + cssicon + '" aria-hidden="true"></span></div>');
//		div.find('[data-toggle="tooltip"]').tooltip();
//	}
//}

//function Warnings() {
//	this.upperCheck = true;
//	this.warningLevel = 0;
//	this.criticalLevel = 0;
//	this.occurancesToScan = 3;
//	this.check = function(data, div, chart) {
//		var criticalCount = 0;
//		var warningCount = 0;
//		var critical = true;
//		var warning = true;
//		var criticalCount = 0;
//		var warningCount = 0;
//		for (var i = data.metricValues.length - 1; i > 0; i--) {
//			if (!critical && !warning) break;
//			var tempdata = data.metricValues[i];
//			if (critical) {
//				if ((this.upperCheck && (tempdata.y > this.criticalLevel)) || (!this.upperCheck && (tempdata.y < this.criticalLevel))) {
//					criticalCount++;
//				}
//				else { //if (data.metricValues.length - i >= this.occurancesToScan) {
//					critical = false;
//				}
//			}
//			if (warning) {
//				if ((this.upperCheck && (tempdata.y > this.warningLevel)) || (!this.upperCheck && (tempdata.y < this.warningLevel))) {
//					warningCount++;
//				}
//				else { //if (data.metricValues.length - i >= this.occurancesToScan) {
//					warning = false;
//				}
//			}
//		}
//		console.log(div + ' warningCount: ' + warningCount + ' | criticalCount: ' + criticalCount);
//		if (criticalCount >= this.occurancesToScan) {
//			$('#' + div).after('<div class="alert alert-danger" role="alert">DANGER! Value: ' + data.metricValues[data.metricValues.length - 1].y + ' is ' + (this.upperCheck ? 'over' : 'under') + ' the threshhold of ' + this.criticalLevel + '. Alert has been observed for ' + criticalCount + ' occurances</div');
//			//chart.options.backgroundColor = "#f2dede";
//		}
//		else if (warningCount >= this.occurancesToScan) {
//			$('#' + div).after('<div class="alert alert-warning" role="alert">WARNING! Value: ' + data.metricValues[data.metricValues.length - 1].y + ' is ' + (this.upperCheck ? 'over' : 'under') + ' the threshhold of ' + this.warningLevel + '. Alert has been observed for ' + warningCount + ' occurances</div');
//			//chart.options.backgroundColor = "#fcf8e3";
//		}
//		else {
//			//chart.options.backgroundColor = "#dff0d8";
//		}
//		if (this.upperCheck) {
//			chart.options.axisY.stripLines = [{
//				startValue: this.warningLevel,
//				endValue: this.criticalLevel,
//				color: "#fcf8e3"
//			}, {
//				startValue: this.criticalLevel,
//				endValue: 9999999,
//				color: "#f2dede"
//			}];
//		}
//		else {
//			chart.options.axisY.stripLines = [{
//				startValue: this.warningLevel,
//				endValue: this.criticalLevel,
//				color: "#fcf8e3"
//			}, {
//				startValue: this.criticalLevel,
//				endValue: -9999999,
//				color: "#f2dede"
//			}];
//		}
//		chart.render();
//	};
//}

//function AddLineChart(chart, data) {
//	chart.options.theme = "theme3";
//	chart.options.backgroundColor = null;
//	chart.options.title = {
//		text: data.id
//	};
//	//chart.options.zoomEnabled = true;
//	chart.options.subtitles = [{
//		text: data.Path
//	}];
//	//chart.options.animationEnabled = true;
//	chart.options.axisX = {
//		valueFormatString: "D/M/Y H:mm",
//		intervalType: "minute"
//	};
//	chart.options.axisY = {
//		includeZero: false,
//		maximum: data.maxy,
//		minimum: 0
//	};
//	chart.options.data = [{
//		xValueType: "dateTime",
//		type: "line",
//		toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
//		lineThickness: 3,
//		name: data.Path,
//		dataPoints: data.metricValues
//			//showInLegend: true
//	}];
//	chart.render();
//}

//function AddLeadsChart(chart, data) {
//	chart.options.theme = "theme3";
//	chart.options.title = {
//		text: data[0].id
//	};
//	chart.options.backgroundColor = null;
//	//chart.options.zoomEnabled = true;
//	//chart.options.animationEnabled = true;
//	chart.options.axisX = {
//		valueFormatString: "D/M/Y H:mm",
//		intervalType: "minute"
//	};
//	chart.options.axisY = {
//		includeZero: false
//	};
//	chart.options.data = [{
//		xValueType: "dateTime",
//		type: "line",
//		toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
//		lineThickness: 3,
//		name: data[0].Path,
//		dataPoints: data[0].metricValues,
//		showInLegend: true
//	}, {
//		xValueType: "dateTime",
//		type: "line",
//		toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
//		lineThickness: 3,
//		name: data[1].Path,
//		dataPoints: data[1].metricValues,
//		showInLegend: true
//	}, {
//		xValueType: "dateTime",
//		type: "line",
//		toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
//		lineThickness: 3,
//		name: data[2].Path,
//		dataPoints: data[2].metricValues,
//		showInLegend: true
//	}, {
//		xValueType: "dateTime",
//		type: "line",
//		toolTipContent: "{label}<br/>{name}: <strong>{y}</strong> - <strong>{x}</strong>",
//		lineThickness: 3,
//		name: data[3].Path,
//		dataPoints: data[3].metricValues,
//		showInLegend: true
//	}];
//	chart.render();
//}

//var easter_egg = new Konami('https://en.wikipedia.org/wiki/Contra_(video_game)');
