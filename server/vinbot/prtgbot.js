var Slack = require('../slack.js');
var Server = require('../loopingserver.js');
var Prtg = require('../servermonitor/prtg.js');
module.exports = function (messageData) {
	var command = messageData.text.split(' ')[0];
	var slackData = Slack.CheckSecure(messageData);
	switch (command) {
		case "pause":
			if (!slackData.validToken || !slackData.adminUser) {
				return "Not authorized";
			}
			return (Prtg.Pause(messageData.text.split(' ')[1]));
			break;
		case "resume":
			if (!slackData.validToken || !slackData.adminUser) {
				return "Not authorized";
			}
			return (Prtg.Resume(messageData.text.split(' ')[1]));
			break;
		case "get":
			switch (messageData.text.split(' ')[1]) {
				case "vaw":
					return HandleGet(Server.data.vawStatus);
					break;
				case "cms":
					return HandleGet(Server.data.cmsStatus);
					break;
				default:
					return "I can only get vaw or cms";
			}
			break;
		default:
			return "PTRG only understands get/resume/pause"
	}
}
function HandleGet(data) {
	var deviceStatus = [];
	for (var i = 0, len = data.metricValues.length; i < len; i++) {
		deviceStatus.push(data.metricValues[i].device + ' ' + data.metricValues[i].status);
	}
	return deviceStatus.join('\n')
}