Declare @DealerID int = 11

Select *
From Dealer
Where DealerID = @DealerID

Select Domain.DomainName, WI.DealerLatitude, WI.DealerLongitude, WI.MakeMyDealID, M.Make,ContactUsEmail, WI.ContactUsADFEmail, WI.EmploymentAppContactEmail, WI.FinanceAppContactEmail, WI.FinanceAppContactADFEmail, WI.QuickQuoteContactEmail, WI.QuickQuoteContactADFEmail, WI.TradeRequestEmail, WI.TradeRequestADFEmail, WI.ServiceContactEmail, WI.ServiceContactADFEmail, WI.PartsContactEmail, WI.PartsContactADFEmail, WI.BodyshopContactEmail,
CASE
  WHEN (32 & W.Flags) = 32 THEN 'True'
  ELSE 'False'
END AS HasUnityWorks,
CASE
  WHEN (64 & W.Flags) = 64 THEN 'True'
  ELSE 'False'
END as HasSisterWorks
From DealerWebSite AS DW
Inner Join WebSiteInfo AS WI
ON DW.WebSiteID = WI.WebSiteID
Inner Join WebSiteDomain AS Domain
ON WI.PrimaryDomainID = Domain.WebSiteDomainID
Inner Join WebSite AS W
ON DW.WebSiteID = W.WebSiteID
Inner Join AutoMake AS M
ON M.AutoMakeID = W.PrimaryAutoMakeID
WHERE DW.DealerID = @DealerID and WI.WebSiteStatus = 1

Select H.*
From Dealer AS D
Inner Join DealerHoursUTC AS H
ON D.DealerID = @DealerID

Select Distinct U.UserName, ISNULL(U.firstname + ' ' + U.lastname,'') Name, U.EmailAddress, UIS.Phone,UIS.UserTitle, UIS.UserDepartment
From [User] AS U
Inner Join UserDealer AS UD
ON U.UserID = UD.UserID
Inner Join UserILMSetting AS UIS
ON U.UserID = UIS.UserID
Where UD.DealerID = @DealerID and U.Status = 'A'

Select R.WebSiteID, R.UrlPattern, R.RedirectUrl, R.StatusCode, R.DomainMatchOnly
From DealerWebSite AS DW
Inner Join DealerWebSite301 AS R
ON DW.WebSiteID = R.WebSiteID
Where DealerID = @DealerID

Select D.DealerID, D.DealerName, DO.DomainName, W.PrimaryAutoMake
From Dealer AS D
LEFT JOIN DealerWebSite AS DW
ON D.DealerID = DW.DealerID
LEFT JOIN Website AS W
ON DW.WebSiteID = W.WebSiteID
LEFT JOIN WebSiteInfo AS WI
ON DW.WebSiteID = WI.WebSiteID AND WI.WebSiteStatus = 1
LEFT JOIN WebSiteDomain AS DO
ON WI.PrimaryDealerID = DO.WebSiteDomainID 


--Primary Make Report
Select D.DealerID, D.DealerName, DO.DomainName, W.PrimaryAutoMake
From Dealer AS D
LEFT JOIN DealerWebSite AS DW
ON D.DealerID = DW.DealerID
LEFT JOIN Website AS W
ON DW.WebSiteID = W.WebSiteID
LEFT JOIN WebSiteInfo AS WI
ON DW.WebSiteID = WI.WebSiteID AND WI.WebSiteStatus = 1
LEFT JOIN WebSiteDomain AS DO
ON WI.PrimaryDealerID = DO.WebSiteDomainID 

--List of Delaers with Multiple Rooftops
Select D.DealerID,D.DealerName, DealershipName, DealershipURL
From RoofTop AS R
Inner Join Dealer AS D
ON R.DealerID = D.DealerID
order by DealerID 











