var express = require('express'),
    comms = require('./server/comms.js'),
    server = require('./server/loopingserver.js'),
    bodyParser = require('body-parser'),
    http = require('http'),
    https = require('https'),
	fs = require("fs"),
	AppSettings = require('./server/settings.js').init();
	
var app = express();

app.set('port', process.env.PORT || AppSettings.Port);

app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(express.static(__dirname + '/public'));


// error handler
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(400).send(err.message);
});

var routes = require('./server/routes.js');
app.use('/', routes);

var webserver = http.createServer(app).listen(app.get('port'), function() {
    comms.initcomms(webserver);
    console.log('server running on '+ app.get('port'))
    server.beginServer();
});