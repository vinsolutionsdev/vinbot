var Slack = require('../slack.js');
var ServerControl = require('../servermonitor/serverControl.js');

module.exports = function (messageData, callback) {
	var command = messageData.text.split(' ')[0];
	var slackData = Slack.CheckSecure(messageData);
	if (!slackData.validToken || !slackData.adminUser) {
		callback("Not authorized");
		return;
	}
	switch (command) {
		case "iisreset":
			ServerControl.ResetIISOnServer(messageData.text.split(' ')[1], callback);
			break;
		case "putinload":
			ServerControl.PutInLoad(messageData.text.split(' ')[1], callback);
			break;
		case "takeoutload":
			ServerControl.RemoveFromLoad(messageData.text.split(' ')[1], callback);
			break;
		default:
			callback("server only understands iisreset/putinload/takeoutload")
	}
}