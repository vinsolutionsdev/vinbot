var express = require('express'),
    server = require('../loopingserver.js'),
    passport = require('passport'),
    BasicStrategy = require('passport-http').BasicStrategy,
	AppSettings = require('../settings.js').init();
var app = express.Router();



function findByUsername(username, fn) {
    for (var i = 0, len = AppSettings.Users.length; i < len; i++) {
        var user = AppSettings.Users[i];
        if (user.username === username) {
            return fn(null, user);
        }
    }
    return fn(null, null);
}

// Use the BasicStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, a username and password), and invoke a callback
//   with a user object.
passport.use(new BasicStrategy({},
    function(username, password, done) {
        // asynchronous verification, for effect...
        process.nextTick(function() {

            // Find the user by username.  If there is no user with the given
            // username, or the password is not correct, set the user to `false` to
            // indicate failure.  Otherwise, return the authenticated `user`.
            findByUsername(username, function(err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }
                if (user.password != password) {
                    return done(null, false);
                }
                return done(null, user);
            })
        });
    }
));

app.use(passport.initialize());

app.get('/getVawResponseTime', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.vawResponseTime);
});
app.get('/getVawLoad', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.vawLoad);
});
app.get('/getCmsResponseTime', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.cmsResponseTime);
});
app.get('/getCmsLoad', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.cmsLoad);
});
app.get('/getCmsStatus', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.cmsStatus);
});
app.get('/getVawStatus', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.vawStatus);
});
app.get('/getWsvcStatus', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.wsvcStatus);
});
app.get('/getleads', passport.authenticate('basic', {
    session: false
}), function(req, res) {
    res.status(200).send(server.data.leadsData);
});
app.get('/getData', passport.authenticate('basic', {
    session: false
}), function (req, res) {
    res.status(200).send(server.data);
});

module.exports = app;