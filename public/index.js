setTimeout(function () {
	window.location.reload(1);
}, 60000);

var vawchart = new CanvasJS.Chart('vaw');
var cmschart = new CanvasJS.Chart('cms');
var leadschart = new CanvasJS.Chart('leads');

var responseTimeWarning = new Warnings();
responseTimeWarning.warningLevel = 200;
responseTimeWarning.criticalLevel = 500;

var leadWarning = new Warnings();
leadWarning.warningLevel = 2;
leadWarning.criticalLevel = 1;
leadWarning.upperCheck = false;

$.ajax({
	url: "/getvaw"
}).done(function (data) {
	data.maxy = 600;
	AddChart(vawchart, data);
	responseTimeWarning.check(data, 'vaw', vawchart);
});
$.ajax({
	url: "/getcms"
}).done(function (data) {
	data.maxy = 600;
	AddChart(cmschart, data);
	responseTimeWarning.check(data, 'cms', cmschart);
});
$.ajax({
	url: "/getleads"
}).done(function (data) {
	AddChart(leadschart, data);
	leadWarning.check(data, 'leads', leadschart);
});

function Warnings() {
	this.upperCheck = true;
	this.warningLevel = 0;
	this.criticalLevel = 0;
	this.occurancesToScan = 3;
	this.check = function (data, div, chart) {
		var criticalCount = 0;
		var warningCount = 0;
		var critical = true;
		var warning = true;
		var criticalCount = 0;
		var warningCount = 0;
		for (var i = data.metricValues.length - 1; i > 0; i--) {
			if (!critical && !warning) break;
			var tempdata = data.metricValues[i];
			if (critical) {
				if ((this.upperCheck && (tempdata.y > this.criticalLevel)) || (!this.upperCheck && (tempdata.y < this.criticalLevel))) {
					criticalCount++;
				}
				else {//if (data.metricValues.length - i >= this.occurancesToScan) {
					critical = false;
				}
			}
			if (warning) {
				if ((this.upperCheck && (tempdata.y > this.warningLevel)) || (!this.upperCheck && (tempdata.y < this.warningLevel))) {
					warningCount++;
				}
				else {//if (data.metricValues.length - i >= this.occurancesToScan) {
					warning = false;
				}
			}
		}
		console.log(div + ' warningCount: ' + warningCount + ' | criticalCount: ' + criticalCount);
		if (criticalCount >= this.occurancesToScan) {
			$('#' + div).after('<div class="alert alert-danger" role="alert">DANGER! Value: ' + data.metricValues[data.metricValues.length - 1].y + ' is ' + (this.upperCheck ? 'over' : 'under') + ' the threshhold of ' + this.criticalLevel + '. Alert has been observed for ' + criticalCount + ' occurances</div');
			//chart.options.backgroundColor = "#f2dede";
		}
		else if (warningCount >= this.occurancesToScan) {
			$('#' + div).after('<div class="alert alert-warning" role="alert">WARNING! Value: ' + data.metricValues[data.metricValues.length - 1].y + ' is ' + (this.upperCheck ? 'over' : 'under') + ' the threshhold of ' + this.warningLevel + '. Alert has been observed for ' + warningCount + ' occurances</div');
			//chart.options.backgroundColor = "#fcf8e3";
		}
		else {
			//chart.options.backgroundColor = "#dff0d8";
		}
		if (this.upperCheck){
			chart.options.axisY.stripLines = [{ startValue:this.warningLevel, endValue:this.criticalLevel, color:"#fcf8e3" },{ startValue:this.criticalLevel, endValue:9999999, color:"#f2dede" }];
		} else {
			chart.options.axisY.stripLines = [{ startValue:this.warningLevel, endValue:this.criticalLevel, color:"#fcf8e3" },{ startValue:this.criticalLevel, endValue:-9999999, color:"#f2dede" }];
		}
		chart.render();
	};
}

function AddChart(chart, data) {
	chart.options.theme = "theme3";
	chart.options.title = {
		text: data.id
	};
	chart.options.zoomEnabled = true;
	chart.options.subtitles = [
		{ text: data.Path }
	];
	chart.options.animationEnabled = true;
	chart.options.axisX = {
		valueFormatString: "D/M/Y H:mm",
		intervalType: "minute"
	};
	chart.options.axisY = {
		includeZero: false,
		maximum: data.maxy
	};
	chart.options.data = [
		{
			xValueType: "dateTime",
			type: "line",
toolTipContent: "{label}<br/>{name}: <strong>{y}</strong>", 
			lineThickness: 3,
			name: data.Path,
			dataPoints: data.metricValues
			//showInLegend: true
		}
	];
	chart.render();
}