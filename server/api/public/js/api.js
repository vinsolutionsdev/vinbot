(function () {
    'use strict';
    var input = document.getElementById('md5testinput');
    document.getElementById('md5testcalculate').addEventListener('click', function (
        event) {
        event.preventDefault();
        document.getElementById('md5testresult').value = md5(input.value);
    });
    input.value = '';
	
	var privatekey = '';
	var publickey = '';
	var temptoken = '';
	var sig = '';
	var dataurl = '';
	var dataurlmd5 = '';
	var finalurl = '';
	
	$('.privatekey').on('input', function() {
		privatekey = $(this).val().trim();
		$('.privatekey').not(this).val(publickey);
		updateurls();
	});
	$('.publickey').on('input', function() {
		publickey = $(this).val().trim();
		$('.publickey').not(this).val(publickey);
		updateurls();
	});
	$('.sig').on('input', function() {
		sig = $(this).val().trim();
		$('.sig').not(this).val(sig);
		updateurls();
	});
	$('.temptoken').on('input', function() {
		temptoken = $(this).val().trim();
		$('.temptoken').not(this).val(temptoken);
		updateurls();
	});
	$('.dataurl').on('input', function() {
		dataurl = $(this).val().trim();
		$('.dataurl').not(this).val(dataurl);
		updateurls();
	}).val('/api/getSites').trigger( "input" );

	$('#myTabs a').click(function (e) {
		e.preventDefault()
		$(this).tab('show');
	})
	
	function updateurls(){
		sig = md5(privatekey + "key=" + publickey);
		$('.sig').val(sig);
		var gettokenurl = '/api/GetToken?key='+publickey+'&sig='+sig;
		$('#gettoken').attr('href', gettokenurl).text(gettokenurl);
		dataurlmd5 = md5(dataurl + privatekey+'key='+publickey+'&token='+temptoken);
		$('.dataurlmd5').val(dataurlmd5);
		finalurl = dataurl+'?key='+publickey+'&token='+temptoken+'&sig='+dataurlmd5
		$('#finalurl').attr('href', finalurl).text(finalurl);
	}
} ());
