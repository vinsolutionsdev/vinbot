SELECT DISTINCT ws.WebSiteID as [WebSiteID], 
                ws.Title as [WebsiteTitle],
				concat('/api/GetSite/', ws.WebSiteID) as URL
FROM   vinmanager..website ws with (nolock)
       INNER JOIN vinmanager..websitedomain wsd with (nolock)
               ON wsd.websiteid = ws.websiteid 
                  AND wsd.ipaddress = '72.21.92.51' 
				  order by ws.websiteid