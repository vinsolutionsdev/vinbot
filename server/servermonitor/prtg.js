var request = require('request');
var comms = require('../comms.js');
var AppSettings = require('../settings.js').init();

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0'

function BuildPRTGURL(action) {
	var prtgauth = "&username=" + AppSettings.PRTGServer.username + "&passhash=" + AppSettings.PRTGServer.passhash;
	var prtgroot = "https://" + AppSettings.PRTGServer.address + "/api/"
	return prtgroot + action + prtgauth;
}

module.exports.PauseID = function (serverid) {
	var url = BuildPRTGURL('pause.htm?id=' + serverid + '&action=0');
	request.post(url);
}

module.exports.ResumeID = function (serverid) {
	var url = BuildPRTGURL('pause.htm?id=' + serverid + '&action=1');
	request.post(url);
}

function GetServerID(serverName) {
	var server = [];
	for (var i = 0; i < serverIDs.length; i++) {
		if (server.length == 0) {
			server = serverIDs[i].servers.filter(function (item) {
				return item.device == serverName;
			});
		}
	}
	if (server.length > 0) return server[0].id
}

module.exports.Pause = function (serverName) {
	var serverID = GetServerID(serverName);
	if (serverID == null) return "Server not found"
	module.exports.PauseID(serverID)
	return 'success'
}
module.exports.Resume = function (serverName) {
	var serverID = GetServerID(serverName);
	if (serverID == null) return "Server not found"
	module.exports.ResumeID(serverID)
	return 'success'
}

var serverIDs = [];

module.exports.Get = function (systemName, callback) {
	systemName = systemName.toLowerCase();
	var system = systems.filter(function (item) {
		return item.name == systemName;
	});
	if (system.length == 1) system = system[0];
	else {
		return "fail: Can't find " + systemName;
	}
	var url = BuildPRTGURL('table.json?content=sensors&output=json&columns=objid,device,status&sortby=device&filter_group=' + system.filter_group + '&filter_sensor=' + system.sensor);

	comms.LogToConsole('requesting: ' + url);
	request({
		url: url
	}, function (error, response, body) {
		if (!error && response.statusCode == 200) {
			var data = JSON.parse(body);
			var retval = new Object();
			retval.id = systemName + '_status';
			retval.metricValues = data.sensors;

			callback(retval); //doing a callback first can cause servers to be empty, but this shouldb't be an issue unless we plan on pausing a server right away
			var servers = serverIDs.filter(function (item) {
				return item.group == systemName;
			});
			if (servers.length == 0) {
				var newservers = { group: systemName, servers: [] };
				for (var i = 0; i < data.sensors.length; i++) {
					newservers.servers.push({ id: data.sensors[i].objid, device: data.sensors[i].device })
				}
				serverIDs.push(newservers);
			}
			return;
		}
		else {
			comms.LogToConsole('error');
			comms.LogToConsole(error);
			comms.LogToConsole('response');
			comms.LogToConsole(response);
			comms.LogToConsole('body');
			comms.LogToConsole(body);
			return;
		}
	});
	return url;
}

var systems = [
	{
		name: 'vaw',
		filter_group: "Dealersites%20-%20Web",
		sensor: "gsi_default.htm"
	}, {
		name: 'cms',
		filter_group: "CMS%20-%20Web",
		sensor: "gsi_default"
	}
];