SELECT Domain.DomainName, WI.DealerLatitude, WI.DealerLongitude, WI.MakeMyDealID, M.Make,ContactUsEmail, WI.ContactUsADFEmail, WI.EmploymentAppContactEmail, WI.FinanceAppContactEmail, WI.FinanceAppContactADFEmail, WI.QuickQuoteContactEmail, WI.QuickQuoteContactADFEmail, WI.TradeRequestEmail, WI.TradeRequestADFEmail, WI.ServiceContactEmail, WI.ServiceContactADFEmail, WI.PartsContactEmail, WI.PartsContactADFEmail, WI.BodyshopContactEmail,WS.PrimaryAutoMake,
CASE
  WHEN (32 & WS.Flags) = 32 THEN 'True'
  ELSE 'False'
END AS HasUnityWorks,
CASE
  WHEN (64 & WS.Flags) = 64 THEN 'True'
  ELSE 'False'
END as HasSisterWorks, ws.WebSiteID as [WebSiteID], 
                ws.Title as [WebsiteTitle], 
                dw.dealers                               AS [DealerCount], 
                ( Stuff((SELECT ', ' + Cast(dealerid AS VARCHAR(max)) 
                         FROM   vinmanager..dealerwebsite with (nolock)
                         WHERE  ( websiteid = ws.websiteid ) 
                         FOR xml path ('')), 1, 2, '') ) AS Dealers, 
                cp.[CustomPages] as [CustomPages]

				,CAST(CASE 
                  WHEN (AISVersion is not null) and HideAIS is null or HideAIS = 0
                     THEN 1 
                  ELSE 0 END AS bit) as AISEnabled,
				concat('/api/GetContent/', ws.WebSiteID) as GetContent,
				concat('/api/GetHours/', ws.WebSiteID) as GetHours
FROM   vinmanager..website ws with (nolock)
       LEFT JOIN (SELECT websiteid, 
                    Count(*) AS Dealers 
             FROM   vinmanager..dealerwebsite with (nolock)
			 where dealerid <> 1
             GROUP  BY websiteid) dw 
         ON ws.websiteid = dw.websiteid 
       LEFT JOIN (SELECT websiteid, 
                    Count(*) AS [CustomPages] 
             FROM   vinmanager..websitecustompage with (nolock)
             WHERE  websitesystempagetypeid IS NULL 
                    AND status = 'A' 
             GROUP  BY websiteid) cp 
         ON ws.websiteid = cp.websiteid 
		 
left Join vinmanager..WebSiteInfo AS WI with (nolock)
ON ws.WebSiteID = WI.WebSiteID
left Join vinmanager..WebSiteDomain AS Domain with (nolock)
ON WI.PrimaryDomainID = Domain.WebSiteDomainID
left Join vinmanager..AutoMake AS M with (nolock)
ON M.AutoMakeID = ws.PrimaryAutoMakeID
where ws.websiteid = {0}