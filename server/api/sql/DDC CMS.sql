declare @dealerid int = 2769
---domain and domain alias
select d.vinsiteid, d.dealerid,S.SiteName,s.SiteDomainName,a.SiteDomainAliasName
From CMS_Site as S
inner join VIN_SiteToDealer as D
on s.SiteID = d.SiteID
left join CMS_SiteDomainAlias as A
on a.SiteID = s.SiteID
where d.DealerID = @dealerid


---redirects
select d.dealerid, d.vinsiteid,s.sitename,r.[from],r.[to],r.sortorder
From CMS_Site as S
inner join VIN_SiteToDealer as D
on s.SiteID = d.SiteID
left join VIN_Redirect AS r
ON R.VinSiteId = D.VinSiteID
where d.DealerID = @dealerid

--hours
Select d.dealerid, d.vinsiteid,s.sitename,h.* 
from View_custom_hours_Joined h
inner join CMS_Site as S on s.siteid = h.NodeSiteID
inner join VIN_SiteToDealer as D
on s.SiteID = d.SiteID
where d.dealerid = @dealerid

--dealerinfo
select dealerinfo.* from View_custom_location_Joined as dealerinfo
inner join CMS_Site as S on s.siteid = dealerinfo.NodeSiteID
inner join VIN_SiteToDealer as D
on s.SiteID = d.SiteID
where d.dealerid = @dealerid


--primary make from here where Name = 'New' -- joins on vinsiteid
Select * from VIN_InventoryFilterContext

---get staff from crm using dealerid


