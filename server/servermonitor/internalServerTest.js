﻿var request = require('request');
var comms = require('../comms.js');
var AppSettings = require('../settings.js').init();
module.exports.CheckVAWServers = function (callback) {
    //var url = 'http://wsvc.qa.vinsolutions.com/MonitoringService/prtg/GetCMSStatus';
    var url = AppSettings.ServerMonitorURL + '/InternalServerTest/CheckVAWServers';
    comms.LogToConsole('requesting: ' + url);
    request({
        url: url
    }, function (error, response, body) {
        HandleSuccessfulRequest(error, response, body, function (result) {
            callback(result);
        });
    });
}


function HandleSuccessfulRequest(error, response, body, callback) {
    if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        var retval = new Object();
        retval.id = 'VawWebsiteStatus';
        retval.metricValues = data;
        callback(retval);
        return;
    }
    else {
        comms.LogToConsole('error');
        comms.LogToConsole(error);
        comms.LogToConsole('response');
        comms.LogToConsole(response);
        comms.LogToConsole('body');
        comms.LogToConsole(body);
    }
}