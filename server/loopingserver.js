var AppDynamics = require('./servermonitor/appdynamics.js');
// var DataBase = require('./database.js');
var Leads = require('./servermonitor/leads.js');
var PRTG = require('./servermonitor/prtg.js');
var InternalServerTest = require('./servermonitor/internalServerTest.js');
var Slack = require('./slack.js');
var comms = require('./comms.js');
var Replication = require('./servermonitor/Replication.js');
var AppSettings = require('./settings.js').init();
exports.data = new Object();
exports.data.cmsBuisnessTransactions = new Array();
exports.data.vawBuisnessTransactions = new Array();
function GetAppDynaicsData(){
	
	//vawBuisnessTransactions 
    var vawBuisnessTransactions = 'Business%20Transaction%20Performance%7CBusiness%20Transactions%7CDealerSites';
    AppDynamics.getMetricItems('Websites 1.0_2.0 - Production', vawBuisnessTransactions, function (resultItems) {
        for (var i = 0, len = resultItems.length; i < len; i++) {
            AppDynamics.getAppDynamics(240, 'Websites 1.0_2.0 - Production', vawBuisnessTransactions+'|' + resultItems[i] + '|Average Response Time (ms)', function (result) {
                if (result == null || result.metricValues.length == 0) return;
                for (var i = 0, len = exports.data.vawBuisnessTransactions.length; i < len; i++) {
                    if (exports.data.vawBuisnessTransactions[i].Path == result.Path) {
                        exports.data.vawBuisnessTransactions.splice(i, 1); break;
                    }
                }
                exports.data.vawBuisnessTransactions.push(result);
            });
        }
    });
	
	//cmsBuisnessTransactions
    var cmsBuisnessTransactions = 'Business%20Transaction%20Performance%7CBusiness%20Transactions%7CKentico';
    AppDynamics.getMetricItems('Websites%203.0%20-%20Production', cmsBuisnessTransactions, function (resultItems) {
        for (var i = 0, len = resultItems.length; i < len; i++) {
            AppDynamics.getAppDynamics(240, 'Websites%203.0%20-%20Production', cmsBuisnessTransactions + '|' + resultItems[i] + '|Average Response Time (ms)', function (result) {
                if (result == null || result.metricValues.length == 0) return;
                for (var i = 0, len = exports.data.cmsBuisnessTransactions.length; i < len; i++) {
                    if (exports.data.cmsBuisnessTransactions[i].Path == result.Path) { exports.data.cmsBuisnessTransactions.splice(i, 1); break; }
                }
                exports.data.cmsBuisnessTransactions.push(result);
            });
        }
        for (var i = 0, len = exports.data.cmsBuisnessTransactions.length; i < len; i++) {
            var tran = exports.data.cmsBuisnessTransactions[i];
            if (tran.metricValues != null && tran.metricValues[tran.metricValues.length - 1].x < new Date().setHours(new Date().getHours() - 4)) {
                tran.metricValues = null;
            }

        };
    });
	
	//vawCpu
    AppDynamics.getAppDynamics(240, 'Websites 1.0_2.0 - Production', 'Application%20Infrastructure%20Performance%7CDealerSites%7CHardware%20Resources%7CCPU%7C%25Busy', function (result) {
        comms.LogToConsole('Call made to vaw AppDynamics Cpu. Result count: ' + result.metricValues.length);
        result.id = "VAW Cpu Data";
        if (result == null || result.metricValues.length == 0) return;
        exports.data.vawCpu = result;
    });
	
	//vawErrors
    AppDynamics.getAppDynamics(240, 'Websites 1.0_2.0 - Production', 'Overall Application Performance|Errors per Minute', function (result) {
        comms.LogToConsole('Call made to vaw AppDynamics Errors. Result count: ' + result.metricValues.length);
        result.id = "VAW Error Data";
        if (result == null || result.metricValues.length == 0) return;
        exports.data.vawErrors = result;
    });
	
	//cmsCpu
    AppDynamics.getAppDynamics(240, 'Websites%203.0%20-%20Production', 'Application%20Infrastructure%20Performance%7CKentico%7CHardware%20Resources%7CCPU%7C%25Busy', function (result) {
        comms.LogToConsole('Call made to cms AppDynamics Cpu. Result count: ' + result.metricValues.length);
        result.id = "CMS Cpu Data";
        if (result == null || result.metricValues.length == 0) return;
        exports.data.cmsCpu = result;
    });
	
	//cmsErrors
    AppDynamics.getAppDynamics(240, 'Websites%203.0%20-%20Production', 'Overall Application Performance|Errors per Minute', function (result) {
        comms.LogToConsole('Call made to cms AppDynamics Errors. Result count: ' + result.metricValues.length);
        result.id = "CMS Error Data";
        if (result == null || result.metricValues.length == 0) return;
        exports.data.cmsErrors = result;
    });
	
	//vawLoad
	AppDynamics.getAppDynamics(240, 'Websites 1.0_2.0 - Production', 'Overall Application Performance|DealerSites|Calls per Minute', function(result) {
		comms.LogToConsole('Call made to vaw AppDynamics Load. Result count: ' + result.metricValues.length);
		result.id = "VAW Load Data";
		if (result == null || result.metricValues.length == 0) return;
		exports.data.vawLoad = result;
	});
	
	//cmsLoad
	AppDynamics.getAppDynamics(240, 'Websites%203.0%20-%20Production', 'Overall%20Application%20Performance%7CKentico%7CCalls%20per%20Minute', function(result) {
		comms.LogToConsole('Call made to cms AppDynamics Load. Result count: ' + result.metricValues.length);
		result.id = "CMS Load Data";
		if (result == null || result.metricValues.length == 0) return;
		exports.data.cmsLoad = result;
	});
	
	//vawResponseTime
	AppDynamics.getAppDynamics(240, 'Websites%201.0_2.0%20-%20Production', 'Overall%20Application%20Performance%7CAverage%20Response%20Time%20%28ms%29', function(result) {
		comms.LogToConsole('Call made to vaw AppDynamics ResponseTime. Result count: ' + result.metricValues.length);
		result.id = "VAW Response Time";
		if (result == null || result.metricValues.length == 0) return;
		exports.data.vawResponseTime = result;
		if (result.metricValues[result.metricValues.length - 1].y > AppSettings.VAWResponseTimeThreshhold) {
			Slack.SendAlert('VAW response time is ' + result.metricValues[result.metricValues.length - 1].y + 'ms', 'vaw');
		}
	});
	
	//cmsResponseTime
	AppDynamics.getAppDynamics(240, 'Websites%203.0%20-%20Production', 'Overall%20Application%20Performance%7CAverage%20Response%20Time%20%28ms%29', function(result) {
		comms.LogToConsole('Call made to cms AppDynamics ResponseTime. Result count: ' + result.metricValues.length);
		result.id = "CMS Response Time";
		if (result == null || result.metricValues.length == 0) return;
		exports.data.cmsResponseTime = result;
		if (result.metricValues[result.metricValues.length - 1].y > AppSettings.CMSResponseTimeThreshhold) {
			Slack.SendAlert('CMS response time is ' + result.metricValues[result.metricValues.length - 1].y + 'ms', 'cms');
		}
	});
	
	//cmsResponseTimePerServer
	AppDynamics.getAppDynamics(1, 'Websites%203.0%20-%20Production', 'Overall%20Application%20Performance%7CKentico%7CIndividual%20Nodes%7C*%7CAverage%20Response%20Time%20%28ms%29', function(result) {
		comms.LogToConsole('Call made to cms AppDynamics ResponseTime Per Server. Result count: ' + result.metricValues.length);
		result.id = "CMS Response Time Per Server";
		if (result == null || result.metricValues.length == 0) return;
		exports.data.cmsResponseTimePerServer = result;
	});
	
	
	//cmsCpuPerServer
    AppDynamics.getAppDynamics(1, 'Websites%203.0%20-%20Production', 'Application%20Infrastructure%20Performance%7CKentico%7CIndividual%20Nodes%7C*%7CHardware%20Resources%7CCPU%7C%25Busy', function (result) {
        comms.LogToConsole('Call made to cms AppDynamics Cpu Per Server. Result count: ' + result.metricValues.length);
        result.id = "CMS Cpu Data Per Server";
        if (result == null || result.metricValues.length == 0) return;
        exports.data.cmsCpuPerServer = result;
    });
}

function GetReplicationData() {
    Replication.getReplicationData(function (result) {
        comms.LogToConsole('Call made for replication. Result count: ' + result.metricValues.length);
        result.id = "Replication Data";
        exports.data.replicaitonStatus = result;
        //for (var i = 0; i < result.length; i++) {
        //    VerifyLeads(result[i]);
        //}
		//console.log(result);
    });
}

function GetLeadsData() {
    Leads.getLeadsData(function (result) {
        comms.LogToConsole('Call made for leads. Result count: ' + result[0].metricValues.length);
        result.id = "Lead Data";
        exports.data.leadsData = result;
    });
    Leads.getBadLeadsData(function (result) {
        comms.LogToConsole('Call made for bad leads.');
        result.id = "Bad Lead Data";
        exports.data.badLeadsData = result;
    });
}

function GetPTRGData() {
    PRTG.Get('cms', function (result) {
        comms.LogToConsole('Call made for prtgData cms. Result count: ' + result.metricValues.length);
        exports.data.cmsStatus = result;
	 	//console.log(result);
    });
    PRTG.Get('vaw', function (result) {
        comms.LogToConsole('Call made for prtgData vaw. Result count: ' + result.metricValues.length);
        exports.data.vawStatus = result;
	 	//console.log(result);
    });
}

function GetInternalServerTestData() {
    InternalServerTest.CheckVAWServers(function (result) {
        comms.LogToConsole('Call made for vaw server status.');
        exports.data.vawServerStatus = result;
	 	//console.log(result);
    });
}


exports.beginServer = function () {
    LoopFunction(GetPTRGData, 10);
    LoopFunction(GetInternalServerTestData, 10);
    LoopFunction(GetLeadsData, 5);
    LoopFunction(GetReplicationData, 5);
    LoopFunction(GetAppDynaicsData, 1);
};

function LoopFunction(functionName, minutes){
    functionName();
    setTimeout(function() {
        LoopFunction(functionName, minutes);
	}, minutes * 60000);
}

function VerifyLeads(leads){
	if (!AppSettings.Slack.Enable) return;
	var type = leads.Path;
	var instances = 0;
	var totalcount = 0;
	for (var i = 10; i < leads.metricValues.length; i++) { 
	    var lead = leads.metricValues[i];
	    if (lead.y > 0) {
	    	instances ++;
	    	totalcount += lead.y;
	    }
	}
	comms.LogToConsole(type + ' | ' + totalcount);
	var notificationSettings = InBuisnessHours() ? AppSettings.Slack.Notifications.BuisnessHours : AppSettings.Slack.Notifications.OutsideBuisnessHours;
	if (type == 'GetUnImportedCMSLeadCounts' && totalcount > notificationSettings.UnImportedCMSLeadThreshhold) { Slack.SendAlert(totalcount + ' unimported CMS leads in the last 4 hours', 'cms'); }
	else if (type == 'GetUnImportedVAWLeadCounts' && totalcount > notificationSettings.UnImportedVAWLeadThreshhold) { Slack.SendAlert(totalcount + ' unimported VAW leads in the last 4 hours', 'vaw'); }
	else if (type == 'GetUnImportedLexusLeadCounts' && totalcount > notificationSettings.UnImportedLexusLeadThreshhold) { Slack.SendAlert(totalcount + ' unimported Lexus leads in the last 4 hours', 'cms'); }
	else if (type == 'GetUnImportedKiaLeadCounts' && totalcount > notificationSettings.UnImportedKiaLeadThreshhold) { Slack.SendAlert(totalcount + ' unimported KIA leads in the last 4 hours', 'vaw'); }
	else if (type == 'GetImportedVAWLeadCounts' && totalcount < notificationSettings.ImportedVAWLeadThreshhold) { Slack.SendAlert(totalcount + ' VAW leads in the last 4 hours', 'vaw'); }
	else if (type == 'GetImportedCMSLeadCounts' && totalcount < notificationSettings.ImportedCMSLeadThreshhold) { Slack.SendAlert(totalcount + ' CMS leads in the last 4 hours', 'cms'); }
	else if (type == 'GetImportedLexusLeadCounts' && totalcount < notificationSettings.ImportedLexusLeadThreshhold) { Slack.SendAlert(totalcount + ' Lexus leads in the last 4 hours', 'cms'); }
	else if (type == 'GetImportedKiaLeadCounts' && totalcount < notificationSettings.ImportedKiaLeadThreshhold) { Slack.SendAlert(totalcount + ' KIA leads in the last 4 hours', 'vaw'); }
}

function InBuisnessHours(){
	var currentHour = new Date().getHours();
	return currentHour < AppSettings.DayEnd && currentHour >= AppSettings.DayStart;
}