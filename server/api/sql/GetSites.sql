SELECT DISTINCT ws.WebSiteID as [WebSite ID], 
                ws.Title as [Website Title], 
                dw.dealers                               AS [Dealer Count], 
                ( Stuff((SELECT ', ' + Cast(dealerid AS VARCHAR(max)) 
                         FROM   vinmanager..dealerwebsite with (nolock)
                         WHERE  ( websiteid = ws.websiteid ) 
                         FOR xml path ('')), 1, 2, '') ) AS Dealers, 
                cp.[Custom Pages] as [Custom Pages]

				,CAST(CASE 
                  WHEN (AISVersion is not null) and HideAIS is null or HideAIS = 0
                     THEN 1 
                  ELSE 0 END AS bit) as AISEnabled
FROM   vinmanager..website ws with (nolock)
       JOIN (SELECT websiteid, 
                    Count(*) AS Dealers 
             FROM   vinmanager..dealerwebsite with (nolock)
			 where dealerid <> 1
             GROUP  BY websiteid) dw 
         ON ws.websiteid = dw.websiteid 
       JOIN (SELECT websiteid, 
                    Count(*) AS [Custom Pages] 
             FROM   vinmanager..websitecustompage with (nolock)
             WHERE  websitesystempagetypeid IS NULL 
                    AND status = 'A' 
             GROUP  BY websiteid) cp 
         ON ws.websiteid = cp.websiteid 
       INNER JOIN vinmanager..websitedomain wsd with (nolock)
               ON wsd.websiteid = ws.websiteid 
                  AND wsd.ipaddress = '72.21.92.51' 
				  order by ws.websiteid