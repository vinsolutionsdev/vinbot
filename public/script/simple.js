﻿'use strict';
angular.module('VINServerMonitor.simple', ['ngRoute'])
    .controller('SimpleCtrl', ['$scope', '$http', '$templateCache',
    function ($scope, $http, $templateCache) {
        $scope.vinbotmessage = 'Ask vinbot a question (try serverstatus)';
        $scope.sendMessage = function () {
            $http({
                url: '/vinbot', 
                method: "GET",
                params: { text: 'vinbot ' +$scope.message, user_name: 'website visitor' }
            }).then(function (res) {
                $scope.vinbotmessage = res.data;
            });
        };
        $scope.init = function () {
            $http.get('/getData').then(function (res) {
                res.data.vawCpuAverage = getAverage(res.data.vawCpu.metricValues);
                res.data.cmsCpuAverage = getAverage(res.data.cmsCpu.metricValues);
                res.data.cmsRespAverage = getAverage(res.data.cmsResponseTime.metricValues);
                res.data.cmsRespAlert = getAlert(res.data.cmsRespAverage, 60, 80);
                res.data.vawCpuAlert = getAlert(res.data.vawCpuAverage, 60, 80);
                res.data.cmsCpuAlert = getAlert(res.data.cmsCpuAverage, 60, 80);
                $scope.data = res.data;
                
                function getAlert(data, warningLevel, alertLevel) {
                    var returnLevel = 0;
                    if (data > warningLevel) returnLevel = 1;
                    if (data > alertLevel) returnLevel = 2;
                    return returnLevel;
                }
                function getAverage(data) {
                    var iterationsToAverageOver = 5;
                    var sum = 0;
                    for (var i = data.length - 1; i > data.length - iterationsToAverageOver - 1; i--) {
                        sum = sum + data[i].y;
                    }
                    return Math.round(sum / iterationsToAverageOver);
                }
            });
        };

    }
]);