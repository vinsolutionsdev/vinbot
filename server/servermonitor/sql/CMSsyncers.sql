--SDBDCMSPR-CAV02.vin.priv
SELECT   REPLACE ( source , 'dbSync: ' , '' ) as [User], IPAddress, count(source) as Count
FROM [Dealersite70_stage_20140311_backup2].[dbo].[CMS_EventLog] with (NOLOCK)
Where source like 'dbSync: %'
and eventtime >= DATEADD(HOUR, -2, GETDATE())
 group by source, IPAddress
order by  count(source) desc