var express = require('express');
var db = require('./database.js');
var app = express.Router();
var auth = require('./auth.js');
var EasyZip = require('easy-zip').EasyZip;

// /getToken?key=2345678&sig=3d737762795be38a5ebfd3c1cab01a57
app.get('/getToken', auth.GetAuth);



function getSites(req, res, next){
	db.RunSQL('GetSitesSimple', [], function (result) {
    	res.status(200).send(result);
    });
}
function getSite(req, res, next){
	db.RunSQL('GetSite', [req.params[0]], function (result) {
    	res.status(200).send(result);
    });
}
function getHours(req, res, next){
	db.RunSQL('GetHours', [req.params[0]], function (result) {
    	res.status(200).send(result);
    });
}
function getContent(req, res, next){
  	var id = req.params[0];
	var zip = new EasyZip();
	zip.zipFolder('\\\\swbdesipr-cav01.vin.priv\\e$\\VinSolutions\\WebApps\\DealerSitesContent\\'+id+'\\',function(){
		zip.writeToResponse(res,'website_'+id); 
	});
}

app.use(express.static(__dirname + '/public'));

app.get('/getSites', auth.auth, getSites);
app.get('/getSite/*', auth.auth, getSite);
app.get('/getContent/*', auth.auth, getContent);
app.get('/getHours/*', auth.auth, getHours);

module.exports = app;