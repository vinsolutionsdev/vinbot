﻿var phantom = require('phantom');
var concurrentScans = 0;

module.exports = function (url, statusCallback, finalCallback) {
    if (concurrentScans > 0) {
        statusCallback({ message: 'I am busy, go away' });
        return;
    }
    var settings = new Object();
    if (url.indexOf("http") == -1) {
        url = 'http://' + url;
    }
    settings.url = url;
    settings.startTime = new Date().getTime();
    concurrentScans++;
    console.log('concurrentScans incresed now: ' + concurrentScans);
    statusCallback({ message: 'Going to scan ' + url });
    scan(settings, function (result) {
        concurrentScans--;
        console.log('concurrentScans decresed now: ' + concurrentScans);
        if (result.status === 'success') {
            result.timing = FormatResult(result.result)
        }
        finalCallback(result);
    });
};

// connectEnd: 1436470880008,
// connectStart: 1436470880008,
// domainLookupEnd: 1436470880008,
// domainLookupStart: 1436470880008,
// fetchStart: 1436470880008,
// navigationStart: 1436470880008,
// requestStart: 1436470880008,
// responseStart: 1436470880008,
// domLoading: 1436470882306,
// responseEnd: 1436470882314,
// domContentLoadedEventStart: 1436470883158,
// domInteractive: 1436470883158,
// domContentLoadedEventEnd: 1436470883237,
// loadEventEnd: 0,
// loadEventStart: 0,
// domComplete: 0,
// redirectEnd: 0,
// redirectStart: 0,
// secureConnectionStart: 0,
// unloadEventEnd: 0,
// unloadEventStart: 0

//https://1-ps.googleusercontent.com/sk/bYSmB63yuhjL_l7bPRuu4R3ENi/www.igvita.com/slides/2012/web-performance-for-the-curious/images/Nx490xnavigation-
function FormatResult(timing) {
    return {
        firstResponse: timing.responseEnd - timing.requestStart,
        frontEnd: timing.loadEventEnd - timing.navigationStart
    };
}

function scan(settings, callback) {
    ParseDomain(settings, callback);
}

function ParseDomain(settings, callback) {
    console.log('getting ' + settings.url);
    phantom.create(function (ph) {
        ph.createPage(function (page) {
            page.open(settings.url, function (status) {
                if (status !== 'success') {
                    callback({ status: status, message: 'Unable to access network' });
                } else {
                    var filename = new Date().getTime() + '.jpeg';
                    page.render('E:\\VinSolutions\\Node\\ServerMonitor\\public\\output\\' + filename, { format: 'jpeg', quality: '80' });
                    filename = '/output/' + filename;
                    //function checkReadyState() {
                    setTimeout(function () {
                        page.evaluate(function () { return window.performance.timing; }, function (result) {
                            //if (result.loadEventEnd > 0) {
                            ph.exit();
                            callback({ status: status, result: result, settings: settings, filename: filename });
                                //}
                                //} else {
                                //    checkReadyState();
                                //}
                        });
                            
                            //console.log(status);
                            //page.evaluate(function () { return window.performance.timing; }, function (result) {
                            //    ph.exit();
                            //    callback({ status: status, result: result, settings: settings });
                            //});
                    }, 10000);
                    //}
                    //checkReadyState();
                }
            });
        });
    }, {
        dnodeOpts: { weak: false }
    });
}
