var express = require('express'),
	vinbot = require('./vinbot.js');
var app = express.Router();
 


app.post('/vinbot', function (req, res) {
    var messageData = { userName: req.body.user_name, text: req.body.text, triggerWord: req.body.trigger_word, channel: req.body.channel_name, userid: req.body.user_id, token: req.body.token };
    if (messageData.userName !== 'slackbot') {
        ParseMessage(messageData, function (result) {
			var retval = new Object();
			if (typeof result.text === 'undefined') {
				retval.text = result;
			} else {
				retval = result;
			}
            return res.status(200).json( retval );
        });
    } else {
        return res.status(200).end();
    }
});

app.get('/vinbot', function (req, res) {
    var messageData = { userName: req.query.user_name, text: req.query.text, triggerWord: req.query.trigger_word, channel: req.body.channel_name };
    if (messageData.userName !== 'slackbot') {
        ParseMessage(messageData, function (result) {
			var retval = new Object();
			if (typeof result === 'undefined') {
				retval.text = result;
			} else {
				retval = result;
			}
            return res.status(200).json( retval );
        });
    } else {
        return res.status(200).end();
    }
});

app.post('/vinbotDirect', function (req, res) {
    var messageData = { userName: req.body.user_name, text: 'vinbot ' + req.body.text, triggerWord: req.body.command, channel: 'website' };
    if (messageData.userName !== 'slackbot') {
        ParseMessage(messageData, function (result) {
            return res.status(200).send(result);
        });
    } else {
        return res.status(200).end();
    }
});

var ParseMessage = function (messageData, callback) {
    messageData.triggerWord = messageData.text.split(' ')[1]; //take second word
    messageData.text = messageData.text.split(' ').slice(2).join(' '); //take third word onwards
    try {
        vinbot[messageData.triggerWord.toLowerCase()](messageData, function (result) {
            callback(result);
        });
    }
	catch (err) {
		if (process.env.NODE_ENV == "dev"){
			callback(encodeURI(err));
			return;
		}
        vinbot['chatting'](messageData, function (result) {
            callback(result);
        });
    }
};



module.exports = app;